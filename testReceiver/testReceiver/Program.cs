﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace testReceiver
{
    class Program
    {
        public static IMS_Receiver_Utility.RDF_Receiver receiver;
        static bool runThread = true;
        static DateTime pastTime;
        static List<double> timePassed;

        static void Main(string[] args)
        {
            receiver = new IMS_Receiver_Utility.RDF_Receiver();

            receiver.connectToReceiver("COM4");

            timePassed = new List<double>();

            //User command section.

            bool keepRunning = true;
            while (keepRunning)
            {
                string command = Console.ReadLine();

                switch (command.ToLower())
                {
                    case "start":
                        System.Threading.Thread getData = new System.Threading.Thread(threadUpdateReceiver);
                        getData.Name = "Rec_Getter";
                        getData.IsBackground = true;
                        getData.Start();
                        break;

                    case "stop":
                        runThread = false;
                        break;

                    case "exit":
                        return;
                }

            }
        }


        static void threadUpdateReceiver()
        {
            while (runThread)
            {
                IMS_Receiver_Utility.ReceiverData data;
                try
                {
                    data = receiver.getReceiverStatusData();



                    DateTime newDataTime = DateTime.Now;
                    TimeSpan passed = newDataTime - pastTime;

                    string avg = "";

                    if (passed.TotalSeconds < 10)
                    {
                        timePassed.Add(passed.TotalSeconds);
                        avg = timePassed.Average().ToString("0.00") + "," + timePassed.Max().ToString("0.00") + "|" + timePassed.Min().ToString("0.00");
                    }

                    Console.WriteLine(DateTime.Now.ToString("HH:mm:ss.ff") + "," + passed.TotalSeconds.ToString("0.00") + "," + avg + "," +
                        data.Frequency.ToString() + "," + data.Status.ToString() + "," + data.SignalStrength.ToString());

                    pastTime = newDataTime;
                }
                catch (Exception error)
                {
                    Console.WriteLine(error.Message);
                }

                System.Threading.Thread.Sleep(950);
            }
        }


    }
}
