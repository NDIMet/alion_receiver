﻿namespace IMS_Receiver_Utility
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.labelR1Carrier = new System.Windows.Forms.Label();
            this.buttonR1Scan = new System.Windows.Forms.Button();
            this.labelR1Communication = new System.Windows.Forms.Label();
            this.panelD1Data = new System.Windows.Forms.Panel();
            this.textBoxD1Pressure = new System.Windows.Forms.TextBox();
            this.textBoxD1SondeID = new System.Windows.Forms.TextBox();
            this.labelD1SondeID = new System.Windows.Forms.Label();
            this.labelD1Alt = new System.Windows.Forms.Label();
            this.labelD1Pressure = new System.Windows.Forms.Label();
            this.textBoxD1Alt = new System.Windows.Forms.TextBox();
            this.textBoxD1Temp = new System.Windows.Forms.TextBox();
            this.labelD1Lon = new System.Windows.Forms.Label();
            this.labelD1RadiosondeData = new System.Windows.Forms.Label();
            this.labelD1Temp = new System.Windows.Forms.Label();
            this.textBoxD1Lon = new System.Windows.Forms.TextBox();
            this.textBoxD1Humidity = new System.Windows.Forms.TextBox();
            this.labelD1Lat = new System.Windows.Forms.Label();
            this.labelD1Humidity = new System.Windows.Forms.Label();
            this.textBoxD1Lat = new System.Windows.Forms.TextBox();
            this.labelR1Data = new System.Windows.Forms.Label();
            this.panelR1Data = new System.Windows.Forms.Panel();
            this.labelScanning = new System.Windows.Forms.Label();
            this.groupBoxReceiverSettings = new System.Windows.Forms.GroupBox();
            this.buttonRCancel = new System.Windows.Forms.Button();
            this.labelLNAGainUnits = new System.Windows.Forms.Label();
            this.labelLNAGain = new System.Windows.Forms.Label();
            this.textBoxLNAGain = new System.Windows.Forms.TextBox();
            this.labelSquelchUnits = new System.Windows.Forms.Label();
            this.labelSquelch = new System.Windows.Forms.Label();
            this.textBoxSquelch = new System.Windows.Forms.TextBox();
            this.labelScanHighUnits = new System.Windows.Forms.Label();
            this.labelScanLowUnits = new System.Windows.Forms.Label();
            this.labelScanHigh = new System.Windows.Forms.Label();
            this.labelScanLow = new System.Windows.Forms.Label();
            this.textBoxScanHigh = new System.Windows.Forms.TextBox();
            this.textBoxScanLow = new System.Windows.Forms.TextBox();
            this.buttonChangeRSettings = new System.Windows.Forms.Button();
            this.textBoxR1Frequency = new System.Windows.Forms.TextBox();
            this.textBoxR1SignalStrength = new System.Windows.Forms.TextBox();
            this.labelR1Frequency = new System.Windows.Forms.Label();
            this.labelR1SignalStrength = new System.Windows.Forms.Label();
            this.textBoxR1AFCMode = new System.Windows.Forms.TextBox();
            this.labelR1AFCMode = new System.Windows.Forms.Label();
            this.panelR1Comms = new System.Windows.Forms.Panel();
            this.labelD1Status = new System.Windows.Forms.Label();
            this.textBoxD1Comms = new System.Windows.Forms.TextBox();
            this.textBoxR1Comms = new System.Windows.Forms.TextBox();
            this.labelR1Status = new System.Windows.Forms.Label();
            this.labelR1ComPort = new System.Windows.Forms.Label();
            this.buttonD1Connect = new System.Windows.Forms.Button();
            this.comboBoxR1ComPort = new System.Windows.Forms.ComboBox();
            this.buttonR1Connect = new System.Windows.Forms.Button();
            this.labelD1ComPort = new System.Windows.Forms.Label();
            this.comboBoxD1ComPort = new System.Windows.Forms.ComboBox();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logDirectoryToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.startLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.stopLoggingToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.panel7 = new System.Windows.Forms.Panel();
            this.buttonlogFileDirectory = new System.Windows.Forms.Button();
            this.buttonStartLogging = new System.Windows.Forms.Button();
            this.labelSystemID = new System.Windows.Forms.Label();
            this.textBoxSystemID = new System.Windows.Forms.TextBox();
            this.textBoxLogDisplay = new System.Windows.Forms.TextBox();
            this.labelMaster = new System.Windows.Forms.Label();
            this.textBoxLogFileName = new System.Windows.Forms.TextBox();
            this.labelFileName = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.groupBoxAutomaticEmail = new System.Windows.Forms.GroupBox();
            this.checkBoxEmailEnabled = new System.Windows.Forms.CheckBox();
            this.buttonEmailTest = new System.Windows.Forms.Button();
            this.radioButtonEmailLock = new System.Windows.Forms.RadioButton();
            this.radioButtonEmailUnlock = new System.Windows.Forms.RadioButton();
            this.labelEmailAddress = new System.Windows.Forms.Label();
            this.textBoxEmailAddress = new System.Windows.Forms.TextBox();
            this.labelCommunications = new System.Windows.Forms.Label();
            this.timerCheckForMidnight = new System.Windows.Forms.Timer(this.components);
            this.timerGarbageCollect = new System.Windows.Forms.Timer(this.components);
            this.pictureBoxR1data = new System.Windows.Forms.PictureBox();
            this.panelD1Data.SuspendLayout();
            this.panelR1Data.SuspendLayout();
            this.groupBoxReceiverSettings.SuspendLayout();
            this.panelR1Comms.SuspendLayout();
            this.menuStrip1.SuspendLayout();
            this.panel7.SuspendLayout();
            this.panel1.SuspendLayout();
            this.groupBoxAutomaticEmail.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxR1data)).BeginInit();
            this.SuspendLayout();
            // 
            // labelR1Carrier
            // 
            this.labelR1Carrier.AutoSize = true;
            this.labelR1Carrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1Carrier.Location = new System.Drawing.Point(34, 40);
            this.labelR1Carrier.Name = "labelR1Carrier";
            this.labelR1Carrier.Size = new System.Drawing.Size(55, 13);
            this.labelR1Carrier.TabIndex = 40;
            this.labelR1Carrier.Text = "CARRIER";
            // 
            // buttonR1Scan
            // 
            this.buttonR1Scan.Enabled = false;
            this.buttonR1Scan.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonR1Scan.Location = new System.Drawing.Point(275, 6);
            this.buttonR1Scan.Name = "buttonR1Scan";
            this.buttonR1Scan.Size = new System.Drawing.Size(97, 37);
            this.buttonR1Scan.TabIndex = 38;
            this.buttonR1Scan.Text = "Start Scanning";
            this.buttonR1Scan.UseVisualStyleBackColor = true;
            this.buttonR1Scan.Click += new System.EventHandler(this.buttonR1Scan_Click);
            // 
            // labelR1Communication
            // 
            this.labelR1Communication.AutoSize = true;
            this.labelR1Communication.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1Communication.Location = new System.Drawing.Point(3, 0);
            this.labelR1Communication.Name = "labelR1Communication";
            this.labelR1Communication.Size = new System.Drawing.Size(170, 20);
            this.labelR1Communication.TabIndex = 34;
            this.labelR1Communication.Text = "COMMUNICATIONS";
            // 
            // panelD1Data
            // 
            this.panelD1Data.BackColor = System.Drawing.SystemColors.Control;
            this.panelD1Data.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelD1Data.Controls.Add(this.textBoxD1Pressure);
            this.panelD1Data.Controls.Add(this.textBoxD1SondeID);
            this.panelD1Data.Controls.Add(this.labelD1SondeID);
            this.panelD1Data.Controls.Add(this.labelD1Alt);
            this.panelD1Data.Controls.Add(this.labelD1Pressure);
            this.panelD1Data.Controls.Add(this.textBoxD1Alt);
            this.panelD1Data.Controls.Add(this.textBoxD1Temp);
            this.panelD1Data.Controls.Add(this.labelD1Lon);
            this.panelD1Data.Controls.Add(this.labelD1RadiosondeData);
            this.panelD1Data.Controls.Add(this.labelD1Temp);
            this.panelD1Data.Controls.Add(this.textBoxD1Lon);
            this.panelD1Data.Controls.Add(this.textBoxD1Humidity);
            this.panelD1Data.Controls.Add(this.labelD1Lat);
            this.panelD1Data.Controls.Add(this.labelD1Humidity);
            this.panelD1Data.Controls.Add(this.textBoxD1Lat);
            this.panelD1Data.Location = new System.Drawing.Point(382, 29);
            this.panelD1Data.Name = "panelD1Data";
            this.panelD1Data.Size = new System.Drawing.Size(481, 154);
            this.panelD1Data.TabIndex = 2;
            // 
            // textBoxD1Pressure
            // 
            this.textBoxD1Pressure.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Pressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Pressure.Location = new System.Drawing.Point(205, 23);
            this.textBoxD1Pressure.Name = "textBoxD1Pressure";
            this.textBoxD1Pressure.ReadOnly = true;
            this.textBoxD1Pressure.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Pressure.TabIndex = 19;
            this.textBoxD1Pressure.Text = "-";
            this.textBoxD1Pressure.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD1SondeID
            // 
            this.textBoxD1SondeID.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1SondeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1SondeID.Location = new System.Drawing.Point(34, 71);
            this.textBoxD1SondeID.Name = "textBoxD1SondeID";
            this.textBoxD1SondeID.ReadOnly = true;
            this.textBoxD1SondeID.Size = new System.Drawing.Size(107, 26);
            this.textBoxD1SondeID.TabIndex = 17;
            this.textBoxD1SondeID.Text = "-";
            this.textBoxD1SondeID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelD1SondeID
            // 
            this.labelD1SondeID.AutoSize = true;
            this.labelD1SondeID.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1SondeID.Location = new System.Drawing.Point(61, 55);
            this.labelD1SondeID.Name = "labelD1SondeID";
            this.labelD1SondeID.Size = new System.Drawing.Size(52, 13);
            this.labelD1SondeID.TabIndex = 18;
            this.labelD1SondeID.Text = "Sonde ID";
            // 
            // labelD1Alt
            // 
            this.labelD1Alt.AutoSize = true;
            this.labelD1Alt.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Alt.Location = new System.Drawing.Point(369, 106);
            this.labelD1Alt.Name = "labelD1Alt";
            this.labelD1Alt.Size = new System.Drawing.Size(59, 13);
            this.labelD1Alt.TabIndex = 30;
            this.labelD1Alt.Text = "Altitude [m]";
            // 
            // labelD1Pressure
            // 
            this.labelD1Pressure.AutoSize = true;
            this.labelD1Pressure.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Pressure.Location = new System.Drawing.Point(205, 7);
            this.labelD1Pressure.Name = "labelD1Pressure";
            this.labelD1Pressure.Size = new System.Drawing.Size(80, 13);
            this.labelD1Pressure.TabIndex = 20;
            this.labelD1Pressure.Text = "Pressure [mbar]";
            // 
            // textBoxD1Alt
            // 
            this.textBoxD1Alt.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Alt.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Alt.Location = new System.Drawing.Point(369, 122);
            this.textBoxD1Alt.Name = "textBoxD1Alt";
            this.textBoxD1Alt.ReadOnly = true;
            this.textBoxD1Alt.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Alt.TabIndex = 29;
            this.textBoxD1Alt.Text = "-";
            this.textBoxD1Alt.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD1Temp
            // 
            this.textBoxD1Temp.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Temp.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Temp.Location = new System.Drawing.Point(205, 71);
            this.textBoxD1Temp.Name = "textBoxD1Temp";
            this.textBoxD1Temp.ReadOnly = true;
            this.textBoxD1Temp.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Temp.TabIndex = 21;
            this.textBoxD1Temp.Text = "-";
            this.textBoxD1Temp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelD1Lon
            // 
            this.labelD1Lon.AutoSize = true;
            this.labelD1Lon.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Lon.Location = new System.Drawing.Point(369, 55);
            this.labelD1Lon.Name = "labelD1Lon";
            this.labelD1Lon.Size = new System.Drawing.Size(54, 13);
            this.labelD1Lon.TabIndex = 28;
            this.labelD1Lon.Text = "Longitude";
            // 
            // labelD1RadiosondeData
            // 
            this.labelD1RadiosondeData.AutoSize = true;
            this.labelD1RadiosondeData.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1RadiosondeData.Location = new System.Drawing.Point(3, 2);
            this.labelD1RadiosondeData.Name = "labelD1RadiosondeData";
            this.labelD1RadiosondeData.Size = new System.Drawing.Size(180, 20);
            this.labelD1RadiosondeData.TabIndex = 16;
            this.labelD1RadiosondeData.Text = "RADIOSONDE DATA";
            // 
            // labelD1Temp
            // 
            this.labelD1Temp.AutoSize = true;
            this.labelD1Temp.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Temp.Location = new System.Drawing.Point(202, 55);
            this.labelD1Temp.Name = "labelD1Temp";
            this.labelD1Temp.Size = new System.Drawing.Size(83, 13);
            this.labelD1Temp.TabIndex = 22;
            this.labelD1Temp.Text = "Temperature [C]";
            // 
            // textBoxD1Lon
            // 
            this.textBoxD1Lon.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Lon.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Lon.Location = new System.Drawing.Point(369, 71);
            this.textBoxD1Lon.Name = "textBoxD1Lon";
            this.textBoxD1Lon.ReadOnly = true;
            this.textBoxD1Lon.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Lon.TabIndex = 27;
            this.textBoxD1Lon.Text = "-";
            this.textBoxD1Lon.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxD1Humidity
            // 
            this.textBoxD1Humidity.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Humidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Humidity.Location = new System.Drawing.Point(205, 122);
            this.textBoxD1Humidity.Name = "textBoxD1Humidity";
            this.textBoxD1Humidity.ReadOnly = true;
            this.textBoxD1Humidity.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Humidity.TabIndex = 23;
            this.textBoxD1Humidity.Text = "-";
            this.textBoxD1Humidity.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelD1Lat
            // 
            this.labelD1Lat.AutoSize = true;
            this.labelD1Lat.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Lat.Location = new System.Drawing.Point(369, 7);
            this.labelD1Lat.Name = "labelD1Lat";
            this.labelD1Lat.Size = new System.Drawing.Size(45, 13);
            this.labelD1Lat.TabIndex = 26;
            this.labelD1Lat.Text = "Latitude";
            // 
            // labelD1Humidity
            // 
            this.labelD1Humidity.AutoSize = true;
            this.labelD1Humidity.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Humidity.Location = new System.Drawing.Point(202, 106);
            this.labelD1Humidity.Name = "labelD1Humidity";
            this.labelD1Humidity.Size = new System.Drawing.Size(80, 13);
            this.labelD1Humidity.TabIndex = 24;
            this.labelD1Humidity.Text = "Humidity [%RH]";
            // 
            // textBoxD1Lat
            // 
            this.textBoxD1Lat.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxD1Lat.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxD1Lat.Location = new System.Drawing.Point(369, 23);
            this.textBoxD1Lat.Name = "textBoxD1Lat";
            this.textBoxD1Lat.ReadOnly = true;
            this.textBoxD1Lat.Size = new System.Drawing.Size(100, 26);
            this.textBoxD1Lat.TabIndex = 25;
            this.textBoxD1Lat.Text = "-";
            this.textBoxD1Lat.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelR1Data
            // 
            this.labelR1Data.AutoSize = true;
            this.labelR1Data.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1Data.Location = new System.Drawing.Point(3, 0);
            this.labelR1Data.Name = "labelR1Data";
            this.labelR1Data.Size = new System.Drawing.Size(153, 20);
            this.labelR1Data.TabIndex = 13;
            this.labelR1Data.Text = "RECEIVER DATA";
            // 
            // panelR1Data
            // 
            this.panelR1Data.BackColor = System.Drawing.SystemColors.Control;
            this.panelR1Data.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelR1Data.Controls.Add(this.labelScanning);
            this.panelR1Data.Controls.Add(this.groupBoxReceiverSettings);
            this.panelR1Data.Controls.Add(this.buttonR1Scan);
            this.panelR1Data.Controls.Add(this.labelR1Carrier);
            this.panelR1Data.Controls.Add(this.textBoxR1Frequency);
            this.panelR1Data.Controls.Add(this.labelR1Data);
            this.panelR1Data.Controls.Add(this.pictureBoxR1data);
            this.panelR1Data.Controls.Add(this.textBoxR1SignalStrength);
            this.panelR1Data.Controls.Add(this.labelR1Frequency);
            this.panelR1Data.Controls.Add(this.labelR1SignalStrength);
            this.panelR1Data.Controls.Add(this.textBoxR1AFCMode);
            this.panelR1Data.Controls.Add(this.labelR1AFCMode);
            this.panelR1Data.Location = new System.Drawing.Point(3, 163);
            this.panelR1Data.Name = "panelR1Data";
            this.panelR1Data.Size = new System.Drawing.Size(377, 325);
            this.panelR1Data.TabIndex = 33;
            // 
            // labelScanning
            // 
            this.labelScanning.AutoSize = true;
            this.labelScanning.BackColor = System.Drawing.Color.Lime;
            this.labelScanning.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.labelScanning.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelScanning.Location = new System.Drawing.Point(179, 13);
            this.labelScanning.Name = "labelScanning";
            this.labelScanning.Size = new System.Drawing.Size(90, 22);
            this.labelScanning.TabIndex = 42;
            this.labelScanning.Text = "Scanning...";
            this.labelScanning.Visible = false;
            // 
            // groupBoxReceiverSettings
            // 
            this.groupBoxReceiverSettings.Controls.Add(this.buttonRCancel);
            this.groupBoxReceiverSettings.Controls.Add(this.labelLNAGainUnits);
            this.groupBoxReceiverSettings.Controls.Add(this.labelLNAGain);
            this.groupBoxReceiverSettings.Controls.Add(this.textBoxLNAGain);
            this.groupBoxReceiverSettings.Controls.Add(this.labelSquelchUnits);
            this.groupBoxReceiverSettings.Controls.Add(this.labelSquelch);
            this.groupBoxReceiverSettings.Controls.Add(this.textBoxSquelch);
            this.groupBoxReceiverSettings.Controls.Add(this.labelScanHighUnits);
            this.groupBoxReceiverSettings.Controls.Add(this.labelScanLowUnits);
            this.groupBoxReceiverSettings.Controls.Add(this.labelScanHigh);
            this.groupBoxReceiverSettings.Controls.Add(this.labelScanLow);
            this.groupBoxReceiverSettings.Controls.Add(this.textBoxScanHigh);
            this.groupBoxReceiverSettings.Controls.Add(this.textBoxScanLow);
            this.groupBoxReceiverSettings.Controls.Add(this.buttonChangeRSettings);
            this.groupBoxReceiverSettings.Location = new System.Drawing.Point(3, 149);
            this.groupBoxReceiverSettings.Name = "groupBoxReceiverSettings";
            this.groupBoxReceiverSettings.Size = new System.Drawing.Size(369, 163);
            this.groupBoxReceiverSettings.TabIndex = 41;
            this.groupBoxReceiverSettings.TabStop = false;
            this.groupBoxReceiverSettings.Text = "Receiver Settings";
            // 
            // buttonRCancel
            // 
            this.buttonRCancel.Enabled = false;
            this.buttonRCancel.Location = new System.Drawing.Point(207, 134);
            this.buttonRCancel.Name = "buttonRCancel";
            this.buttonRCancel.Size = new System.Drawing.Size(75, 23);
            this.buttonRCancel.TabIndex = 13;
            this.buttonRCancel.Text = "Cancel";
            this.buttonRCancel.UseVisualStyleBackColor = true;
            this.buttonRCancel.Click += new System.EventHandler(this.buttonRCancel_Click);
            // 
            // labelLNAGainUnits
            // 
            this.labelLNAGainUnits.AutoSize = true;
            this.labelLNAGainUnits.Location = new System.Drawing.Point(269, 50);
            this.labelLNAGainUnits.Name = "labelLNAGainUnits";
            this.labelLNAGainUnits.Size = new System.Drawing.Size(26, 13);
            this.labelLNAGainUnits.TabIndex = 12;
            this.labelLNAGainUnits.Text = "[dB]";
            // 
            // labelLNAGain
            // 
            this.labelLNAGain.AutoSize = true;
            this.labelLNAGain.Location = new System.Drawing.Point(184, 32);
            this.labelLNAGain.Name = "labelLNAGain";
            this.labelLNAGain.Size = new System.Drawing.Size(53, 13);
            this.labelLNAGain.TabIndex = 11;
            this.labelLNAGain.Text = "LNA Gain";
            // 
            // textBoxLNAGain
            // 
            this.textBoxLNAGain.Location = new System.Drawing.Point(164, 48);
            this.textBoxLNAGain.MaxLength = 3;
            this.textBoxLNAGain.Name = "textBoxLNAGain";
            this.textBoxLNAGain.ReadOnly = true;
            this.textBoxLNAGain.Size = new System.Drawing.Size(100, 20);
            this.textBoxLNAGain.TabIndex = 10;
            this.textBoxLNAGain.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelSquelchUnits
            // 
            this.labelSquelchUnits.AutoSize = true;
            this.labelSquelchUnits.Location = new System.Drawing.Point(111, 129);
            this.labelSquelchUnits.Name = "labelSquelchUnits";
            this.labelSquelchUnits.Size = new System.Drawing.Size(34, 13);
            this.labelSquelchUnits.TabIndex = 9;
            this.labelSquelchUnits.Text = "[dBm]";
            // 
            // labelSquelch
            // 
            this.labelSquelch.AutoSize = true;
            this.labelSquelch.Location = new System.Drawing.Point(32, 110);
            this.labelSquelch.Name = "labelSquelch";
            this.labelSquelch.Size = new System.Drawing.Size(46, 13);
            this.labelSquelch.TabIndex = 8;
            this.labelSquelch.Text = "Squelch";
            // 
            // textBoxSquelch
            // 
            this.textBoxSquelch.Location = new System.Drawing.Point(5, 126);
            this.textBoxSquelch.MaxLength = 4;
            this.textBoxSquelch.Name = "textBoxSquelch";
            this.textBoxSquelch.ReadOnly = true;
            this.textBoxSquelch.Size = new System.Drawing.Size(100, 20);
            this.textBoxSquelch.TabIndex = 7;
            this.textBoxSquelch.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelScanHighUnits
            // 
            this.labelScanHighUnits.AutoSize = true;
            this.labelScanHighUnits.Location = new System.Drawing.Point(111, 89);
            this.labelScanHighUnits.Name = "labelScanHighUnits";
            this.labelScanHighUnits.Size = new System.Drawing.Size(35, 13);
            this.labelScanHighUnits.TabIndex = 6;
            this.labelScanHighUnits.Text = "[MHz]";
            // 
            // labelScanLowUnits
            // 
            this.labelScanLowUnits.AutoSize = true;
            this.labelScanLowUnits.Location = new System.Drawing.Point(112, 50);
            this.labelScanLowUnits.Name = "labelScanLowUnits";
            this.labelScanLowUnits.Size = new System.Drawing.Size(35, 13);
            this.labelScanLowUnits.TabIndex = 5;
            this.labelScanLowUnits.Text = "[MHz]";
            // 
            // labelScanHigh
            // 
            this.labelScanHigh.AutoSize = true;
            this.labelScanHigh.Location = new System.Drawing.Point(6, 70);
            this.labelScanHigh.Name = "labelScanHigh";
            this.labelScanHigh.Size = new System.Drawing.Size(110, 13);
            this.labelScanHigh.TabIndex = 4;
            this.labelScanHigh.Text = "Scan High Frequency";
            // 
            // labelScanLow
            // 
            this.labelScanLow.AutoSize = true;
            this.labelScanLow.Location = new System.Drawing.Point(6, 31);
            this.labelScanLow.Name = "labelScanLow";
            this.labelScanLow.Size = new System.Drawing.Size(108, 13);
            this.labelScanLow.TabIndex = 3;
            this.labelScanLow.Text = "Scan Low Frequency";
            // 
            // textBoxScanHigh
            // 
            this.textBoxScanHigh.Location = new System.Drawing.Point(5, 86);
            this.textBoxScanHigh.MaxLength = 8;
            this.textBoxScanHigh.Name = "textBoxScanHigh";
            this.textBoxScanHigh.ReadOnly = true;
            this.textBoxScanHigh.Size = new System.Drawing.Size(100, 20);
            this.textBoxScanHigh.TabIndex = 2;
            this.textBoxScanHigh.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxScanLow
            // 
            this.textBoxScanLow.Location = new System.Drawing.Point(6, 47);
            this.textBoxScanLow.MaxLength = 8;
            this.textBoxScanLow.Name = "textBoxScanLow";
            this.textBoxScanLow.ReadOnly = true;
            this.textBoxScanLow.Size = new System.Drawing.Size(100, 20);
            this.textBoxScanLow.TabIndex = 1;
            this.textBoxScanLow.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // buttonChangeRSettings
            // 
            this.buttonChangeRSettings.Enabled = false;
            this.buttonChangeRSettings.Location = new System.Drawing.Point(288, 134);
            this.buttonChangeRSettings.Name = "buttonChangeRSettings";
            this.buttonChangeRSettings.Size = new System.Drawing.Size(75, 23);
            this.buttonChangeRSettings.TabIndex = 0;
            this.buttonChangeRSettings.Text = "Change";
            this.buttonChangeRSettings.UseVisualStyleBackColor = true;
            this.buttonChangeRSettings.Click += new System.EventHandler(this.buttonChangeRSettings_Click);
            // 
            // textBoxR1Frequency
            // 
            this.textBoxR1Frequency.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxR1Frequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxR1Frequency.Location = new System.Drawing.Point(12, 86);
            this.textBoxR1Frequency.Name = "textBoxR1Frequency";
            this.textBoxR1Frequency.ReadOnly = true;
            this.textBoxR1Frequency.Size = new System.Drawing.Size(100, 26);
            this.textBoxR1Frequency.TabIndex = 5;
            this.textBoxR1Frequency.Text = "-";
            this.textBoxR1Frequency.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxR1SignalStrength
            // 
            this.textBoxR1SignalStrength.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxR1SignalStrength.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxR1SignalStrength.Location = new System.Drawing.Point(130, 86);
            this.textBoxR1SignalStrength.Name = "textBoxR1SignalStrength";
            this.textBoxR1SignalStrength.ReadOnly = true;
            this.textBoxR1SignalStrength.Size = new System.Drawing.Size(100, 26);
            this.textBoxR1SignalStrength.TabIndex = 6;
            this.textBoxR1SignalStrength.Text = "-";
            this.textBoxR1SignalStrength.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelR1Frequency
            // 
            this.labelR1Frequency.AutoSize = true;
            this.labelR1Frequency.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1Frequency.Location = new System.Drawing.Point(21, 70);
            this.labelR1Frequency.Name = "labelR1Frequency";
            this.labelR1Frequency.Size = new System.Drawing.Size(88, 13);
            this.labelR1Frequency.TabIndex = 8;
            this.labelR1Frequency.Text = "Frequency [MHz]";
            // 
            // labelR1SignalStrength
            // 
            this.labelR1SignalStrength.AutoSize = true;
            this.labelR1SignalStrength.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1SignalStrength.Location = new System.Drawing.Point(131, 70);
            this.labelR1SignalStrength.Name = "labelR1SignalStrength";
            this.labelR1SignalStrength.Size = new System.Drawing.Size(109, 13);
            this.labelR1SignalStrength.TabIndex = 9;
            this.labelR1SignalStrength.Text = "Signal Strength [dBm]";
            // 
            // textBoxR1AFCMode
            // 
            this.textBoxR1AFCMode.BackColor = System.Drawing.SystemColors.Info;
            this.textBoxR1AFCMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBoxR1AFCMode.Location = new System.Drawing.Point(254, 86);
            this.textBoxR1AFCMode.Name = "textBoxR1AFCMode";
            this.textBoxR1AFCMode.ReadOnly = true;
            this.textBoxR1AFCMode.Size = new System.Drawing.Size(100, 26);
            this.textBoxR1AFCMode.TabIndex = 14;
            this.textBoxR1AFCMode.Text = "-";
            this.textBoxR1AFCMode.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelR1AFCMode
            // 
            this.labelR1AFCMode.AutoSize = true;
            this.labelR1AFCMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1AFCMode.Location = new System.Drawing.Point(274, 70);
            this.labelR1AFCMode.Name = "labelR1AFCMode";
            this.labelR1AFCMode.Size = new System.Drawing.Size(57, 13);
            this.labelR1AFCMode.TabIndex = 15;
            this.labelR1AFCMode.Text = "AFC Mode";
            // 
            // panelR1Comms
            // 
            this.panelR1Comms.BackColor = System.Drawing.SystemColors.Control;
            this.panelR1Comms.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panelR1Comms.Controls.Add(this.labelR1Communication);
            this.panelR1Comms.Controls.Add(this.labelD1Status);
            this.panelR1Comms.Controls.Add(this.textBoxD1Comms);
            this.panelR1Comms.Controls.Add(this.textBoxR1Comms);
            this.panelR1Comms.Controls.Add(this.labelR1Status);
            this.panelR1Comms.Controls.Add(this.labelR1ComPort);
            this.panelR1Comms.Controls.Add(this.buttonD1Connect);
            this.panelR1Comms.Controls.Add(this.comboBoxR1ComPort);
            this.panelR1Comms.Controls.Add(this.buttonR1Connect);
            this.panelR1Comms.Controls.Add(this.labelD1ComPort);
            this.panelR1Comms.Controls.Add(this.comboBoxD1ComPort);
            this.panelR1Comms.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.panelR1Comms.Location = new System.Drawing.Point(3, 29);
            this.panelR1Comms.Name = "panelR1Comms";
            this.panelR1Comms.Size = new System.Drawing.Size(377, 135);
            this.panelR1Comms.TabIndex = 32;
            // 
            // labelD1Status
            // 
            this.labelD1Status.AutoSize = true;
            this.labelD1Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1Status.Location = new System.Drawing.Point(251, 78);
            this.labelD1Status.Name = "labelD1Status";
            this.labelD1Status.Size = new System.Drawing.Size(50, 13);
            this.labelD1Status.TabIndex = 35;
            this.labelD1Status.Text = "STATUS";
            // 
            // textBoxD1Comms
            // 
            this.textBoxD1Comms.BackColor = System.Drawing.Color.Red;
            this.textBoxD1Comms.Location = new System.Drawing.Point(228, 94);
            this.textBoxD1Comms.Name = "textBoxD1Comms";
            this.textBoxD1Comms.ReadOnly = true;
            this.textBoxD1Comms.Size = new System.Drawing.Size(87, 20);
            this.textBoxD1Comms.TabIndex = 34;
            this.textBoxD1Comms.Text = "Offline";
            this.textBoxD1Comms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // textBoxR1Comms
            // 
            this.textBoxR1Comms.BackColor = System.Drawing.Color.Red;
            this.textBoxR1Comms.Location = new System.Drawing.Point(228, 49);
            this.textBoxR1Comms.Name = "textBoxR1Comms";
            this.textBoxR1Comms.ReadOnly = true;
            this.textBoxR1Comms.Size = new System.Drawing.Size(87, 20);
            this.textBoxR1Comms.TabIndex = 33;
            this.textBoxR1Comms.Text = "Offline";
            this.textBoxR1Comms.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // labelR1Status
            // 
            this.labelR1Status.AutoSize = true;
            this.labelR1Status.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1Status.Location = new System.Drawing.Point(251, 33);
            this.labelR1Status.Name = "labelR1Status";
            this.labelR1Status.Size = new System.Drawing.Size(50, 13);
            this.labelR1Status.TabIndex = 32;
            this.labelR1Status.Text = "STATUS";
            // 
            // labelR1ComPort
            // 
            this.labelR1ComPort.AutoSize = true;
            this.labelR1ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelR1ComPort.Location = new System.Drawing.Point(3, 34);
            this.labelR1ComPort.Name = "labelR1ComPort";
            this.labelR1ComPort.Size = new System.Drawing.Size(99, 13);
            this.labelR1ComPort.TabIndex = 4;
            this.labelR1ComPort.Text = "Receiver COM Port";
            // 
            // buttonD1Connect
            // 
            this.buttonD1Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonD1Connect.Location = new System.Drawing.Point(130, 92);
            this.buttonD1Connect.Name = "buttonD1Connect";
            this.buttonD1Connect.Size = new System.Drawing.Size(81, 21);
            this.buttonD1Connect.TabIndex = 31;
            this.buttonD1Connect.Text = "Connect";
            this.buttonD1Connect.UseVisualStyleBackColor = true;
            this.buttonD1Connect.Click += new System.EventHandler(this.buttonD1Connect_Click);
            // 
            // comboBoxR1ComPort
            // 
            this.comboBoxR1ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxR1ComPort.FormattingEnabled = true;
            this.comboBoxR1ComPort.Location = new System.Drawing.Point(3, 50);
            this.comboBoxR1ComPort.Name = "comboBoxR1ComPort";
            this.comboBoxR1ComPort.Size = new System.Drawing.Size(121, 21);
            this.comboBoxR1ComPort.TabIndex = 1;
            // 
            // buttonR1Connect
            // 
            this.buttonR1Connect.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonR1Connect.Location = new System.Drawing.Point(130, 49);
            this.buttonR1Connect.Name = "buttonR1Connect";
            this.buttonR1Connect.Size = new System.Drawing.Size(81, 21);
            this.buttonR1Connect.TabIndex = 2;
            this.buttonR1Connect.Text = "Connect";
            this.buttonR1Connect.UseVisualStyleBackColor = true;
            this.buttonR1Connect.Click += new System.EventHandler(this.buttonR1Connect_Click);
            // 
            // labelD1ComPort
            // 
            this.labelD1ComPort.AutoSize = true;
            this.labelD1ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelD1ComPort.Location = new System.Drawing.Point(3, 77);
            this.labelD1ComPort.Name = "labelD1ComPort";
            this.labelD1ComPort.Size = new System.Drawing.Size(97, 13);
            this.labelD1ComPort.TabIndex = 12;
            this.labelD1ComPort.Text = "Decoder COM Port";
            // 
            // comboBoxD1ComPort
            // 
            this.comboBoxD1ComPort.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBoxD1ComPort.FormattingEnabled = true;
            this.comboBoxD1ComPort.Location = new System.Drawing.Point(3, 93);
            this.comboBoxD1ComPort.Name = "comboBoxD1ComPort";
            this.comboBoxD1ComPort.Size = new System.Drawing.Size(121, 21);
            this.comboBoxD1ComPort.TabIndex = 11;
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.logDirectoryToolStripMenuItem,
            this.startLoggingToolStripMenuItem,
            this.stopLoggingToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // logDirectoryToolStripMenuItem
            // 
            this.logDirectoryToolStripMenuItem.Name = "logDirectoryToolStripMenuItem";
            this.logDirectoryToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.logDirectoryToolStripMenuItem.Text = "Log Directory";
            // 
            // startLoggingToolStripMenuItem
            // 
            this.startLoggingToolStripMenuItem.Name = "startLoggingToolStripMenuItem";
            this.startLoggingToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.startLoggingToolStripMenuItem.Text = "Start Logging";
            // 
            // stopLoggingToolStripMenuItem
            // 
            this.stopLoggingToolStripMenuItem.Name = "stopLoggingToolStripMenuItem";
            this.stopLoggingToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.stopLoggingToolStripMenuItem.Text = "Stop Logging";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(145, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem1});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(865, 24);
            this.menuStrip1.TabIndex = 1;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem1
            // 
            this.fileToolStripMenuItem1.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem1});
            this.fileToolStripMenuItem1.Name = "fileToolStripMenuItem1";
            this.fileToolStripMenuItem1.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem1.Text = "File";
            // 
            // exitToolStripMenuItem1
            // 
            this.exitToolStripMenuItem1.Name = "exitToolStripMenuItem1";
            this.exitToolStripMenuItem1.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem1.Text = "Exit";
            this.exitToolStripMenuItem1.Click += new System.EventHandler(this.exitToolStripMenuItem1_Click);
            // 
            // panel7
            // 
            this.panel7.BackColor = System.Drawing.SystemColors.Control;
            this.panel7.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel7.Controls.Add(this.buttonlogFileDirectory);
            this.panel7.Controls.Add(this.buttonStartLogging);
            this.panel7.Controls.Add(this.labelSystemID);
            this.panel7.Controls.Add(this.textBoxSystemID);
            this.panel7.Controls.Add(this.textBoxLogDisplay);
            this.panel7.Controls.Add(this.labelMaster);
            this.panel7.Controls.Add(this.textBoxLogFileName);
            this.panel7.Controls.Add(this.labelFileName);
            this.panel7.Location = new System.Drawing.Point(382, 286);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(481, 202);
            this.panel7.TabIndex = 4;
            // 
            // buttonlogFileDirectory
            // 
            this.buttonlogFileDirectory.Location = new System.Drawing.Point(439, 48);
            this.buttonlogFileDirectory.Name = "buttonlogFileDirectory";
            this.buttonlogFileDirectory.Size = new System.Drawing.Size(28, 23);
            this.buttonlogFileDirectory.TabIndex = 9;
            this.buttonlogFileDirectory.Text = "...";
            this.buttonlogFileDirectory.UseVisualStyleBackColor = true;
            this.buttonlogFileDirectory.Click += new System.EventHandler(this.buttonlogFileDirectory_Click);
            // 
            // buttonStartLogging
            // 
            this.buttonStartLogging.Location = new System.Drawing.Point(205, 12);
            this.buttonStartLogging.Name = "buttonStartLogging";
            this.buttonStartLogging.Size = new System.Drawing.Size(75, 23);
            this.buttonStartLogging.TabIndex = 8;
            this.buttonStartLogging.Text = "Start";
            this.buttonStartLogging.UseVisualStyleBackColor = true;
            this.buttonStartLogging.Click += new System.EventHandler(this.buttonStartLogging_Click);
            // 
            // labelSystemID
            // 
            this.labelSystemID.AutoSize = true;
            this.labelSystemID.Location = new System.Drawing.Point(306, 17);
            this.labelSystemID.Name = "labelSystemID";
            this.labelSystemID.Size = new System.Drawing.Size(55, 13);
            this.labelSystemID.TabIndex = 7;
            this.labelSystemID.Text = "System ID";
            // 
            // textBoxSystemID
            // 
            this.textBoxSystemID.Location = new System.Drawing.Point(367, 15);
            this.textBoxSystemID.Name = "textBoxSystemID";
            this.textBoxSystemID.Size = new System.Drawing.Size(100, 20);
            this.textBoxSystemID.TabIndex = 6;
            this.textBoxSystemID.Text = "Default";
            this.textBoxSystemID.TextChanged += new System.EventHandler(this.textBoxSystemID_TextChanged);
            // 
            // textBoxLogDisplay
            // 
            this.textBoxLogDisplay.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.textBoxLogDisplay.AutoCompleteMode = System.Windows.Forms.AutoCompleteMode.Append;
            this.textBoxLogDisplay.BackColor = System.Drawing.SystemColors.Window;
            this.textBoxLogDisplay.Location = new System.Drawing.Point(2, 83);
            this.textBoxLogDisplay.Multiline = true;
            this.textBoxLogDisplay.Name = "textBoxLogDisplay";
            this.textBoxLogDisplay.ReadOnly = true;
            this.textBoxLogDisplay.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBoxLogDisplay.Size = new System.Drawing.Size(474, 114);
            this.textBoxLogDisplay.TabIndex = 5;
            // 
            // labelMaster
            // 
            this.labelMaster.AutoSize = true;
            this.labelMaster.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelMaster.Location = new System.Drawing.Point(8, 0);
            this.labelMaster.Name = "labelMaster";
            this.labelMaster.Size = new System.Drawing.Size(144, 20);
            this.labelMaster.TabIndex = 2;
            this.labelMaster.Text = "DATA LOGGING";
            // 
            // textBoxLogFileName
            // 
            this.textBoxLogFileName.Location = new System.Drawing.Point(3, 51);
            this.textBoxLogFileName.Name = "textBoxLogFileName";
            this.textBoxLogFileName.ReadOnly = true;
            this.textBoxLogFileName.Size = new System.Drawing.Size(430, 20);
            this.textBoxLogFileName.TabIndex = 1;
            // 
            // labelFileName
            // 
            this.labelFileName.AutoSize = true;
            this.labelFileName.Location = new System.Drawing.Point(9, 35);
            this.labelFileName.Name = "labelFileName";
            this.labelFileName.Size = new System.Drawing.Size(75, 13);
            this.labelFileName.TabIndex = 0;
            this.labelFileName.Text = "Log File Name";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.Control;
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.groupBoxAutomaticEmail);
            this.panel1.Controls.Add(this.buttonEmailTest);
            this.panel1.Controls.Add(this.radioButtonEmailLock);
            this.panel1.Controls.Add(this.radioButtonEmailUnlock);
            this.panel1.Controls.Add(this.labelEmailAddress);
            this.panel1.Controls.Add(this.textBoxEmailAddress);
            this.panel1.Controls.Add(this.labelCommunications);
            this.panel1.Location = new System.Drawing.Point(382, 181);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(481, 108);
            this.panel1.TabIndex = 5;
            // 
            // groupBoxAutomaticEmail
            // 
            this.groupBoxAutomaticEmail.Controls.Add(this.checkBoxEmailEnabled);
            this.groupBoxAutomaticEmail.Enabled = false;
            this.groupBoxAutomaticEmail.Location = new System.Drawing.Point(309, 7);
            this.groupBoxAutomaticEmail.Name = "groupBoxAutomaticEmail";
            this.groupBoxAutomaticEmail.Size = new System.Drawing.Size(158, 91);
            this.groupBoxAutomaticEmail.TabIndex = 9;
            this.groupBoxAutomaticEmail.TabStop = false;
            this.groupBoxAutomaticEmail.Text = "Automatic Email (Midnight)";
            // 
            // checkBoxEmailEnabled
            // 
            this.checkBoxEmailEnabled.AutoSize = true;
            this.checkBoxEmailEnabled.Location = new System.Drawing.Point(49, 41);
            this.checkBoxEmailEnabled.Name = "checkBoxEmailEnabled";
            this.checkBoxEmailEnabled.Size = new System.Drawing.Size(65, 17);
            this.checkBoxEmailEnabled.TabIndex = 0;
            this.checkBoxEmailEnabled.Text = "Enabled";
            this.checkBoxEmailEnabled.UseVisualStyleBackColor = true;
            this.checkBoxEmailEnabled.CheckedChanged += new System.EventHandler(this.checkBoxEmailEnabled_CheckedChanged);
            // 
            // buttonEmailTest
            // 
            this.buttonEmailTest.Enabled = false;
            this.buttonEmailTest.Location = new System.Drawing.Point(219, 79);
            this.buttonEmailTest.Name = "buttonEmailTest";
            this.buttonEmailTest.Size = new System.Drawing.Size(75, 23);
            this.buttonEmailTest.TabIndex = 8;
            this.buttonEmailTest.Text = "Test";
            this.buttonEmailTest.UseVisualStyleBackColor = true;
            this.buttonEmailTest.Click += new System.EventHandler(this.buttonEmailTest_Click);
            // 
            // radioButtonEmailLock
            // 
            this.radioButtonEmailLock.AutoSize = true;
            this.radioButtonEmailLock.Checked = true;
            this.radioButtonEmailLock.Location = new System.Drawing.Point(145, 23);
            this.radioButtonEmailLock.Name = "radioButtonEmailLock";
            this.radioButtonEmailLock.Size = new System.Drawing.Size(49, 17);
            this.radioButtonEmailLock.TabIndex = 7;
            this.radioButtonEmailLock.TabStop = true;
            this.radioButtonEmailLock.Text = "Lock";
            this.radioButtonEmailLock.UseVisualStyleBackColor = true;
            this.radioButtonEmailLock.CheckedChanged += new System.EventHandler(this.radioButtonEmailLock_CheckedChanged);
            // 
            // radioButtonEmailUnlock
            // 
            this.radioButtonEmailUnlock.AutoSize = true;
            this.radioButtonEmailUnlock.Location = new System.Drawing.Point(200, 23);
            this.radioButtonEmailUnlock.Name = "radioButtonEmailUnlock";
            this.radioButtonEmailUnlock.Size = new System.Drawing.Size(59, 17);
            this.radioButtonEmailUnlock.TabIndex = 6;
            this.radioButtonEmailUnlock.Text = "Unlock";
            this.radioButtonEmailUnlock.UseVisualStyleBackColor = true;
            this.radioButtonEmailUnlock.CheckedChanged += new System.EventHandler(this.radioButtonEmailUnlock_CheckedChanged);
            // 
            // labelEmailAddress
            // 
            this.labelEmailAddress.AutoSize = true;
            this.labelEmailAddress.Location = new System.Drawing.Point(4, 37);
            this.labelEmailAddress.Name = "labelEmailAddress";
            this.labelEmailAddress.Size = new System.Drawing.Size(73, 13);
            this.labelEmailAddress.TabIndex = 5;
            this.labelEmailAddress.Text = "Email Address";
            // 
            // textBoxEmailAddress
            // 
            this.textBoxEmailAddress.Location = new System.Drawing.Point(3, 53);
            this.textBoxEmailAddress.Name = "textBoxEmailAddress";
            this.textBoxEmailAddress.ReadOnly = true;
            this.textBoxEmailAddress.Size = new System.Drawing.Size(291, 20);
            this.textBoxEmailAddress.TabIndex = 4;
            // 
            // labelCommunications
            // 
            this.labelCommunications.AutoSize = true;
            this.labelCommunications.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCommunications.Location = new System.Drawing.Point(3, 0);
            this.labelCommunications.Name = "labelCommunications";
            this.labelCommunications.Size = new System.Drawing.Size(165, 20);
            this.labelCommunications.TabIndex = 3;
            this.labelCommunications.Text = "DATA REPORTING";
            // 
            // timerCheckForMidnight
            // 
            this.timerCheckForMidnight.Interval = 60000;
            this.timerCheckForMidnight.Tick += new System.EventHandler(this.timerCheckForMidnight_Tick);
            // 
            // timerGarbageCollect
            // 
            this.timerGarbageCollect.Enabled = true;
            this.timerGarbageCollect.Interval = 600000;
            this.timerGarbageCollect.Tick += new System.EventHandler(this.timerGarbageCollect_Tick);
            // 
            // pictureBoxR1data
            // 
            this.pictureBoxR1data.Image = global::IMS_Receiver_Utility.Properties.Resources.LED_Green_off;
            this.pictureBoxR1data.Location = new System.Drawing.Point(9, 35);
            this.pictureBoxR1data.Name = "pictureBoxR1data";
            this.pictureBoxR1data.Size = new System.Drawing.Size(20, 18);
            this.pictureBoxR1data.TabIndex = 37;
            this.pictureBoxR1data.TabStop = false;
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(865, 488);
            this.Controls.Add(this.panelR1Data);
            this.Controls.Add(this.panelR1Comms);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panel7);
            this.Controls.Add(this.menuStrip1);
            this.Controls.Add(this.panelD1Data);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Main";
            this.Text = "600111 InterMet Receiver Utility  Version 1.06";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Main_FormClosing);
            this.Load += new System.EventHandler(this.Main_Load);
            this.panelD1Data.ResumeLayout(false);
            this.panelD1Data.PerformLayout();
            this.panelR1Data.ResumeLayout(false);
            this.panelR1Data.PerformLayout();
            this.groupBoxReceiverSettings.ResumeLayout(false);
            this.groupBoxReceiverSettings.PerformLayout();
            this.panelR1Comms.ResumeLayout(false);
            this.panelR1Comms.PerformLayout();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.panel7.ResumeLayout(false);
            this.panel7.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.groupBoxAutomaticEmail.ResumeLayout(false);
            this.groupBoxAutomaticEmail.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxR1data)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonR1Connect;
        private System.Windows.Forms.ComboBox comboBoxR1ComPort;
        private System.Windows.Forms.Label labelR1ComPort;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.TextBox textBoxR1SignalStrength;
        private System.Windows.Forms.TextBox textBoxR1Frequency;
        private System.Windows.Forms.Label labelR1SignalStrength;
        private System.Windows.Forms.Label labelR1Frequency;
        private System.Windows.Forms.Label labelD1ComPort;
        private System.Windows.Forms.ComboBox comboBoxD1ComPort;
        private System.Windows.Forms.Label labelD1RadiosondeData;
        private System.Windows.Forms.Label labelR1AFCMode;
        private System.Windows.Forms.TextBox textBoxR1AFCMode;
        private System.Windows.Forms.Label labelR1Data;
        private System.Windows.Forms.Label labelD1Temp;
        private System.Windows.Forms.TextBox textBoxD1Temp;
        private System.Windows.Forms.Label labelD1Pressure;
        private System.Windows.Forms.TextBox textBoxD1Pressure;
        private System.Windows.Forms.Label labelD1SondeID;
        private System.Windows.Forms.TextBox textBoxD1SondeID;
        private System.Windows.Forms.Button buttonD1Connect;
        private System.Windows.Forms.Label labelD1Alt;
        private System.Windows.Forms.TextBox textBoxD1Alt;
        private System.Windows.Forms.Label labelD1Lon;
        private System.Windows.Forms.TextBox textBoxD1Lon;
        private System.Windows.Forms.Label labelD1Lat;
        private System.Windows.Forms.TextBox textBoxD1Lat;
        private System.Windows.Forms.Label labelD1Humidity;
        private System.Windows.Forms.TextBox textBoxD1Humidity;
        private System.Windows.Forms.Panel panelR1Comms;
        private System.Windows.Forms.Panel panelD1Data;
        private System.Windows.Forms.Panel panelR1Data;
        private System.Windows.Forms.Label labelR1Status;
        private System.Windows.Forms.Label labelR1Communication;
        private System.Windows.Forms.ToolStripMenuItem logDirectoryToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem startLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem stopLoggingToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.PictureBox pictureBoxR1data;
        private System.Windows.Forms.Button buttonR1Scan;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem1;
        private System.Windows.Forms.Label labelD1Status;
        private System.Windows.Forms.TextBox textBoxD1Comms;
        private System.Windows.Forms.TextBox textBoxR1Comms;
        private System.Windows.Forms.Label labelR1Carrier;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.TextBox textBoxLogFileName;
        private System.Windows.Forms.Label labelFileName;
        private System.Windows.Forms.Label labelMaster;
        private System.Windows.Forms.TextBox textBoxLogDisplay;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton radioButtonEmailLock;
        private System.Windows.Forms.RadioButton radioButtonEmailUnlock;
        private System.Windows.Forms.Label labelEmailAddress;
        private System.Windows.Forms.TextBox textBoxEmailAddress;
        private System.Windows.Forms.Label labelCommunications;
        private System.Windows.Forms.Button buttonlogFileDirectory;
        private System.Windows.Forms.Button buttonStartLogging;
        private System.Windows.Forms.Label labelSystemID;
        private System.Windows.Forms.TextBox textBoxSystemID;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem1;
        private System.Windows.Forms.Timer timerCheckForMidnight;
        private System.Windows.Forms.GroupBox groupBoxReceiverSettings;
        private System.Windows.Forms.Button buttonChangeRSettings;
        private System.Windows.Forms.Label labelSquelchUnits;
        private System.Windows.Forms.Label labelSquelch;
        private System.Windows.Forms.TextBox textBoxSquelch;
        private System.Windows.Forms.Label labelScanHighUnits;
        private System.Windows.Forms.Label labelScanLowUnits;
        private System.Windows.Forms.Label labelScanHigh;
        private System.Windows.Forms.Label labelScanLow;
        private System.Windows.Forms.TextBox textBoxScanHigh;
        private System.Windows.Forms.TextBox textBoxScanLow;
        private System.Windows.Forms.Button buttonRCancel;
        private System.Windows.Forms.Label labelLNAGainUnits;
        private System.Windows.Forms.Label labelLNAGain;
        private System.Windows.Forms.TextBox textBoxLNAGain;
        private System.Windows.Forms.Button buttonEmailTest;
        private System.Windows.Forms.GroupBox groupBoxAutomaticEmail;
        private System.Windows.Forms.CheckBox checkBoxEmailEnabled;
        private System.Windows.Forms.Label labelScanning;
        private System.Windows.Forms.Timer timerGarbageCollect;
    }
}

