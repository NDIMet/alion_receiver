﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMS_Receiver_Utility
{
    public class iMet_1_Decoder
    {
        //Public Fields
        public System.IO.Ports.SerialPort COMPort;
        public DecoderData sDecoderData;

        //Private Fields

        //Public Methods
        public bool connectToDecoder(string COMPortName)
        {
            this.COMPort = new System.IO.Ports.SerialPort(COMPortName, 9600, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.COMPort.ReadTimeout = 2000;

            try
            {
                this.COMPort.Open();
                this.COMPort.DiscardInBuffer();

                if (getDecoderPacket() == null) 
                {
                    this.COMPort.Dispose();
                    return false;
                }
                
                return true;

            }
            catch
            {
                this.COMPort.Dispose();
                return false;
            }
        }

        public DecoderData getRadiosondeData() 
        {
            byte[] bDecoderData = getDecoderPacket();
            return getDecoderData(bDecoderData);
        }

        private DecoderData getDecoderData(byte[] newData) 
        {
            DecoderData newDecoderData = new DecoderData();
            //Initialize Decoder Data
            newDecoderData.Sonde_ID = null;
            newDecoderData.pressure = -999.99;
            newDecoderData.temperature = -999.99;
            newDecoderData.humidity = -999.99;

            if (newData == null) 
            {
                return newDecoderData;
            }

            switch (newData.Length) 
            {
                case 5: //No Decoder Data
                    break;
                case 181:   //New Sonde Format
                    string sondeID = "";
                    for (int i = 13; i < 23; i++) 
                    {
                        if (newData[i] == 0x00) 
                        {
                            break;
                        }
                        sondeID += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Sonde_ID = sondeID;

                    //PTU Data
                    byte[] bPressure = new byte[]{newData[60],newData[61],newData[62],0x00};
                    UInt32 pressure = BitConverter.ToUInt32(bPressure, 0);
                    newDecoderData.pressure = (double)pressure / 1000.0;
                    byte[] bTemperature = new byte[] { newData[78], newData[79], newData[80], 0x00 };
                    UInt32 temperature = BitConverter.ToUInt32(bTemperature, 0);
                    newDecoderData.temperature = (double)temperature / 1000.0 - 273.15;
                    byte[] bHumidity = new byte[] { newData[53], newData[54] };
                    UInt16 humidity = BitConverter.ToUInt16(bHumidity, 0);
                    newDecoderData.humidity = (double)humidity / 100.0;

                    //GPS Data

                    Int32 latitude = BitConverter.ToInt32(newData, 128);
                    newDecoderData.latitude = latitude * 180.0 / Math.Pow(2, 31);
                    
                    Int32 longitude = BitConverter.ToInt32(newData, 132);
                    newDecoderData.longitude = longitude * 180.0 / Math.Pow(2, 31);

                    Int32 altitude = BitConverter.ToInt32(newData,136);
                    newDecoderData.altitude = altitude / 1000.0;
                    
                    break;
                case 190:   //Old Sonde Format
                    sondeID = "";
                    for (int i = 13; i < 23; i++) 
                    {
                        if (newData[i] == 0x00) 
                        {
                            break;
                        }
                        sondeID += Convert.ToChar(newData[i]);
                    }
                    newDecoderData.Sonde_ID = sondeID;

                    bPressure = new byte[]{newData[69],newData[70],newData[71],0x00};
                    pressure = BitConverter.ToUInt32(bPressure, 0);
                    newDecoderData.pressure = (double)pressure / 1000.0;
                    bTemperature = new byte[] { newData[87], newData[88], newData[89], 0x00 };
                    temperature = BitConverter.ToUInt32(bTemperature, 0);
                    newDecoderData.temperature = (double)temperature / 1000.0 - 273.15;
                    bHumidity = new byte[] { newData[62], newData[63] };
                    humidity = BitConverter.ToUInt16(bHumidity, 0);
                    newDecoderData.humidity = (double)humidity / 100.0;

                    //GPS Data

                    latitude = BitConverter.ToInt32(newData, 137);
                    newDecoderData.latitude = latitude * 180.0 / Math.Pow(2, 31);

                    longitude = BitConverter.ToInt32(newData, 141);
                    newDecoderData.longitude = longitude * 180.0 / Math.Pow(2, 31);

                    altitude = BitConverter.ToInt32(newData, 145);
                    newDecoderData.altitude = altitude / 1000.0;
                    break;
                default:
                    break;
            }
            return newDecoderData;
        }

        private byte[] getDecoderPacket()
        {
            List<byte> bList = new List<byte>();

            while (true) //Wait for first byte
            {
                byte newByte;

                try { newByte = (byte)this.COMPort.ReadByte(); }
                catch { goto BadPacket; }

                if (newByte == 0x10)
                {
                    try
                    {
                        newByte = (byte)this.COMPort.ReadByte();
                    }
                    catch { goto BadPacket; }

                    if (newByte == 0x00)    //No Packet
                    {
                        bList.Add(0x10);
                        bList.Add(0x00);
                        goto NoRadiosondeData;
                    }
                    if (newByte == 0xB0) //Radiosonde Packet (No GPS)
                    {
                        bList.Add(0x10);
                        bList.Add(0xB0);
                        goto OldFirmwareRadiosondePacket;
                    }
                    if (newByte == 0xB9) 
                    {
                        bList.Add(0x10);
                        bList.Add(0xB9);
                        goto NewFirmwareRadiosondePacket;
                    }
                }
            }

        NoRadiosondeData:
            while (true)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                bList.Add(newByte);
                if (newByte == 0x04)
                {
                    goto BuildPacket;
                }
            }

        OldFirmwareRadiosondePacket:
            while(bList.Count < 181)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                if (newByte == 0x10 && bList[bList.Count - 1] == 0x10)
                {
                    //Do not add byte
                }
                else
                {
                    bList.Add(newByte);
                }
            }

            goto BuildPacket;

        NewFirmwareRadiosondePacket:
            while (bList.Count < 190)
            {
                byte newByte;
                try
                {
                    newByte = (byte)this.COMPort.ReadByte();
                }
                catch { goto BadPacket; }

                if (newByte == 0x10 && bList[bList.Count - 1] == 0x10)
                {
                    //Do not add byte
                }
                else
                {
                    bList.Add(newByte);
                }
            }

            goto BuildPacket;

        BuildPacket:
            byte[] sondePacket = new byte[bList.Count];
            for (int i = 0; i < bList.Count; i++)
            {
                sondePacket[i] = bList[i];
            }
            return sondePacket;

        BadPacket:
            bool COM_OK = checkComPort();
            if (!COM_OK) 
            {
                resetCOMPort();
            }
            return null;

        }//End method getDecoderPacket

        private bool checkComPort()
        {
            try
            {
                this.COMPort.Write("0");
            }
            catch 
            {
                return false;
            }
            return true;
        }

        private void resetCOMPort() 
        {
            if (this.COMPort.IsOpen) 
            {
                this.COMPort.Close();
                while (!connectToDecoder(this.COMPort.PortName)) { }
            }
        }

    }//End class iMet1_Decoder

    public struct DecoderData
    {
        public string Sonde_ID;
        public double pressure;
        public double temperature;
        public double humidity;
        public double latitude;
        public double longitude;
        public double altitude;
    }
}
