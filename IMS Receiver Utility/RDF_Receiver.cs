﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMS_Receiver_Utility
{
    public class RDF_Receiver
    {   //Public Methods
        public bool connectToReceiver(string COMPortName) 
        {
            this.COMPort = new System.IO.Ports.SerialPort(COMPortName, 19200, System.IO.Ports.Parity.None, 8, System.IO.Ports.StopBits.One);
            this.COMPort.NewLine = "\r\n";

            try
            {
                this.COMPort.Open();
                this.COMPort.DiscardInBuffer();
                this.COMPort.ReadTimeout = 5000;
                this.sReceiverSettings = getReceiverSettings();
                this.sReceiverData = getReceiverStatusData();
 
                return true;
            }
            catch 
            {
                this.COMPort.Dispose();
                return false;
            }
        }

        public ReceiverData getReceiverStatusData() 
        {
            string response = null;
                
            //Get Frequency
            this.COMPort.Write("/RF?\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            this.COMPort.NewLine = "\r\n\r\n";
            response = this.COMPort.ReadLine();
            string sFrequency = response.Replace("Receiver Frequency: ", null);
            try
            {
                this.sReceiverData.Frequency = double.Parse(sFrequency) / 1000000.0;
            }
            catch
            {
                this.sReceiverData.Frequency = 0.0;
            }

            //Get Signal Strength
            this.COMPort.Write("/RQ?\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            response = this.COMPort.ReadLine();
            string sSignalStrength = response.Replace("RSSI: ", null);
            try
            {
                this.sReceiverData.SignalStrength = int.Parse(sSignalStrength);
            }
            catch 
            {
                this.sReceiverData.SignalStrength = 0;
            }
            
            //Get Offset
            this.COMPort.Write("/RO?\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            response = this.COMPort.ReadLine();
            string sOffset = response.Replace("Offset: RO=", null);
            try
            {
                this.sReceiverData.Offset = int.Parse(sOffset);
            }
            catch 
            {
                this.sReceiverData.Offset = 0;
            }

            //Get AFC
            this.COMPort.Write("/RA?\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            this.COMPort.NewLine = "\r\n\r\n";
            response = this.COMPort.ReadLine();
            if (response.Contains("AFC: 1"))
            {
                this.sReceiverData.AFC_ON = true;
            }
            else if(response.Contains("AFC: 0"))
            {
                this.sReceiverData.AFC_ON = false;
            }

            return this.sReceiverData;
        }

        public ReceiverSettings getReceiverSettings()
        {
            string response = null;
            
            this.COMPort.Write("/CRQL?\r");
            this.COMPort.NewLine = "\r\n";

            response = this.COMPort.ReadLine();
            response = this.COMPort.ReadLine();

            int LNA_Gain = int.Parse(response.Replace("LNA Gain: ", null));

            this.sReceiverSettings.LNA_Gain = LNA_Gain;

            return this.sReceiverSettings;
        }

        public void toggleAFC(bool AFC_ON) 
        {
            string response = null;
            if (AFC_ON)
            {
                this.COMPort.Write("/RA=1\r");
                this.COMPort.NewLine = "\r\n";
                response = this.COMPort.ReadLine();
                this.COMPort.NewLine = "\r\n\r\n";
                response = this.COMPort.ReadLine();
            }
            else 
            {
                this.COMPort.Write("/RA=0\r");
                this.COMPort.NewLine = "\r\n";
                response = this.COMPort.ReadLine();
                this.COMPort.NewLine = "\r\n\r\n";
                response = this.COMPort.ReadLine();
            }
        }

        //Private Methods
        //Send a command to the receiver
        public void sendCommand(string command) 
        {
            string toSend = addChecksum("/"+command)+"\r";
            this.COMPort.Write(toSend);
        }

        //Get a response from the receiver
        public string getResponse() 
        {
            try
            {
                return this.COMPort.ReadLine();
            }
            catch 
            {
                return null;
            }
        }

        //Add XOR checksum to validate data
        private string addChecksum(string message) 
        {
            char[] cMessage = message.ToCharArray();
            byte checksum = 0;
            for (int i = 0; i < cMessage.Length; i++) 
            {
                byte bChar = Convert.ToByte(cMessage[i]);
                checksum ^= bChar;
            }
            return message + checksum.ToString("X2");
        }

        public void scanFrequencies(double F1,double F2)
        {
            string response = null;

            string frequency1 = (F1 * 1000.0).ToString("0000000");
            string frequency2 = (F2 * 1000.0).ToString("0000000");

            this.COMPort.Write("/RPL" + frequency1 + "U" + frequency2 + "\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();

            do
            {
                try
                {
                    this.COMPort.ReadTimeout = 2000;
                    response = this.COMPort.ReadLine();
                }
                catch
                {
                    break;
                }
            }
            while (response.Contains("TUNE UP")||response.Contains("TUNE DOWN"));

            toggleAFC(false);
        }

        public void goToFrequency(double frequency)
        {
            //Turn off AFC
            toggleAFC(false);

            string response = null;
            string sFrequency = (frequency * 1000000.0).ToString("0000000000");

            this.COMPort.Write("/RF=" + sFrequency + "\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            this.COMPort.NewLine = "\r\n\r\n";
            response = this.COMPort.ReadLine();
        }

        public void stepFrequency(long stepSizekHz) 
        {
            string response = null;
            string sStepSize = stepSizekHz.ToString("+00000;-00000");
            this.COMPort.Write("/RF" + sStepSize + "\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();
        }

        public bool receiverConnected() 
        {
            if (this.COMPort!=null && this.COMPort.IsOpen)
            {
                return true;
            }
            else return false;
        }

        public void setTestMode(bool TestMode_ON) 
        {
            string command = "/IRTM=";
            if (TestMode_ON)
            {
                command += "1";
            }
            else 
            {
                command += "0";
            }
            this.COMPort.Write(command + "\r");
            
            this.COMPort.NewLine = "\r\n";
            this.COMPort.ReadLine();
            this.COMPort.NewLine = "\r\n\r\n";
            this.COMPort.ReadLine();
        }

        public void setLNAGain(int LNA_Gain) 
        {
            this.COMPort.ReadTimeout = System.Threading.Timeout.Infinite;

            if (Math.Abs(LNA_Gain) > 50) 
            {
                throw new SystemException("Invalid LNA Gain. Must be +/- 50 dB");
            }
            
            setTestMode(true);

            string sLNA_Gain = LNA_Gain.ToString("+00;-00");
            this.COMPort.Write("/CRQL=" + sLNA_Gain + "\r");

            string response = "";
            this.COMPort.NewLine = "\r\n";
            while (!response.Contains("Flash Data Updated")) 
            {
                response = this.COMPort.ReadLine();
            }

            setTestMode(false);
        }

        private int getOffset() 
        {
            int offset = 0;
            string response = null;

            this.COMPort.Write("/RO?\r");
            this.COMPort.NewLine = "\r\n";
            response = this.COMPort.ReadLine();

            return offset;
        }

        //Public Fields
        public ReceiverData sReceiverData;
        public ReceiverSettings sReceiverSettings;

        public System.IO.Ports.SerialPort COMPort;
    }

    public struct ReceiverData
    {
        public double Frequency;
        public int SignalStrength;
        public int Offset;
        public UInt32 Status;
        public bool AFC_ON;
    }

    public struct ReceiverSettings 
    {
        public int LNA_Gain;                 //LNA Gain in dB
    }
}
