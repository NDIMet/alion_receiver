﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace IMS_Receiver_Utility
{
    public partial class Main : Form
    {
        //Global Objects
        public Setup CurrentSetup;
        public RDF_Receiver Receiver1;
        public iMet_1_Decoder Decoder1;

        public System.IO.StreamWriter LogFile;
        
        //Delegates
        delegate void delegateUpdateCommsDisplay(bool comboBoxEnabled, bool StatusConnected, string Status);
        delegate void delegateUpdataReceiverDisplay(ReceiverData ReceiverData);
        delegate void delegateUpdateReceiverSettings(ReceiverSettings ReceiverSettings, bool ChangeButton, bool CancelButton);
        delegate void delegateUpdateDecoderDisplay(DecoderData DecoderData);
        delegate void delegateWriteToLogFileDisplay(string newData);
        delegate void delegateUpdateLogFileName(string fileName);

        System.Threading.Thread Decoder1Thread;
        System.Threading.Thread Receiver1Thread;

        private volatile bool _Receiver1Scanning = false;
        private volatile bool _Decoder1Monitor = false;
        private volatile bool _ReceiverThreadRunning = false;

        private volatile bool _RecordData = false;
        
        private readonly object _WriteFileLocker = new object();
        private readonly object _ReceiverCOMMSLocker = new object();

        //Methods
        public Main()
        {
            InitializeComponent();
        }

        private void Main_Load(object sender, EventArgs e)
        {
            //Get saved setup information
            this.CurrentSetup = new Setup();
            this.CurrentSetup.getSetupFromFile("Setup.cfg");

            //Initialize Combo Boxes
            this.comboBoxR1ComPort.DataSource = System.IO.Ports.SerialPort.GetPortNames();
            this.comboBoxR1ComPort.Text = this.CurrentSetup.Receiver1COMPortName;
            this.comboBoxR1ComPort.SelectedValueChanged += new EventHandler(comboBoxR1ComPort_SelectedValueChanged);

            this.comboBoxD1ComPort.DataSource = System.IO.Ports.SerialPort.GetPortNames();
            this.comboBoxD1ComPort.Text = this.CurrentSetup.Decoder1COMPortName;
            this.comboBoxD1ComPort.SelectedValueChanged += new EventHandler(comboBoxD1ComPort_SelectedValueChanged);

            this.textBoxLogFileName.Text = this.buildLogFileName();
            this.textBoxEmailAddress.Text = this.CurrentSetup.EmailAddress;
            this.checkBoxEmailEnabled.Checked = this.CurrentSetup.AutomaticEmail;

            this.textBoxScanLow.Text = this.CurrentSetup.ReceiverFrequencyLow.ToString("0000.000");
            this.textBoxScanHigh.Text = this.CurrentSetup.ReceiverFrequencyHigh.ToString("0000.000");
            this.textBoxSquelch.Text = this.CurrentSetup.ReceiverSquelch.ToString();

            timerCheckForMidnight.Enabled = true;
            timerCheckForMidnight.Start();
        }

        void comboBoxD1ComPort_SelectedValueChanged(object sender, EventArgs e)
        {
            this.CurrentSetup.Decoder1COMPortName = this.comboBoxD1ComPort.SelectedValue.ToString();
        }

        void comboBoxR1ComPort_SelectedValueChanged(object sender, EventArgs e)
        {
            this.CurrentSetup.Receiver1COMPortName = this.comboBoxR1ComPort.SelectedValue.ToString();
        }

        private void buttonR1Connect_Click(object sender, EventArgs e)
        {
            if (this.buttonR1Connect.Text.Contains("Connect"))
            {
                System.Threading.Thread ConnectToReceiver1Thread = new System.Threading.Thread(ConnectToReceiver);
                ConnectToReceiver1Thread.Start();
                if (textBoxR1Comms.Text.Contains("Online")) 
                {
                    buttonChangeRSettings.Enabled = true;
                }
            }
            else 
            {
                MessageBox.Show("Disconnect not implemented yet. Will be added later.");
                //Disconnect from receiver
            }
        }

        void ConnectToReceiver() 
        {
                    RDF_Receiver newReceiver = new RDF_Receiver();
                    if (newReceiver.connectToReceiver(this.CurrentSetup.Receiver1COMPortName))
                    {
                        this.Receiver1 = newReceiver;
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel),new object[] {false,false,"Online"});
                        this.Invoke(new delegateUpdateReceiverSettings(this.UpdateReceiverSettingsDisplay), new object[] { this.Receiver1.sReceiverSettings,true,false});

                        lock (_ReceiverCOMMSLocker) 
                        {
                            this._ReceiverThreadRunning = true;
                            this._Receiver1Scanning = false;
                        }
                        this.Receiver1Thread = new System.Threading.Thread(ReceiverCommunicationLoop);
                        this.Receiver1Thread.Start();
                    }
                    else 
                    {
                        MessageBox.Show("Could not detect receiver!");
                    }
        }

        void ReceiverCommunicationLoop() 
        {
            System.Threading.Thread.CurrentThread.IsBackground = true;
            
            bool ScanningLoopEnable;

            bool ReceiverLoopEnable;

            while (true)
            {
            
            DetermineStatus:
                lock (_ReceiverCOMMSLocker)
                {
                    ScanningLoopEnable = this._Receiver1Scanning;
                    ReceiverLoopEnable = this._ReceiverThreadRunning;
                }
                if (!ReceiverLoopEnable) 
                {
                    return;
                }
                if (ScanningLoopEnable)
                {
                    goto ScanningLoop;
                }
                else 
                {
                    goto MonitorLoop;
                }
            
            MonitorLoop:
                ReceiverData data = new ReceiverData();
                try
                {
                    data = this.Receiver1.getReceiverStatusData();
                    this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { data });
                    this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                    System.Threading.Thread.Sleep(2700);
                }
                catch
                {
                    this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                    this.Receiver1.COMPort.DiscardInBuffer();
                    this.receiverCOMMSErrorHandler();
                    goto DetermineStatus;
                }

                if (this._RecordData && data.SignalStrength > this.CurrentSetup.ReceiverSquelch)
                {
                    string currentTime = DateTime.Now.ToString("HH:mm:ss");
                    string newRecord = currentTime + ",[Receiver]," + data.Frequency.ToString("#.000") + "," + data.SignalStrength.ToString("+000;-000") + "," + data.Offset.ToString("+000;-000") + "\r\n";

                    try { this.Invoke(new delegateWriteToLogFileDisplay(this.UpdateLogFileDisplay), new object[] { newRecord }); }
                    catch { }

                    if (this.LogFile != null)
                    {
                        lock (_WriteFileLocker)
                        {

                            try
                            {
                                this.LogFile.Write(newRecord);
                            }
                            catch (Exception writeError)
                            {
                                buttonStartLogging_Click(null, null);
                                MessageBox.Show(writeError.Message, "Receiver Log Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }

                        System.Threading.Thread.Sleep(950);
                    }
                }
                goto DetermineStatus;

            ScanningLoop:
                ReceiverData newData = new ReceiverData();
                int stepSize = 100; //kHz
                
                //Get Status to notify
                try
                {
                    newData = this.Receiver1.getReceiverStatusData();
                    this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { newData });
                    this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                }
                catch
                {
                    this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                    this.Receiver1.COMPort.DiscardInBuffer();
                    this.receiverCOMMSErrorHandler();
                    goto DetermineStatus;
                }

                if (newData.Frequency != this.CurrentSetup.ReceiverFrequencyLow)
                {
                    try
                    {
                        this.Receiver1.goToFrequency(this.CurrentSetup.ReceiverFrequencyLow);
                        newData = this.Receiver1.getReceiverStatusData();
                        this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { newData });
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                    }
                    catch
                    {
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                        this.Receiver1.COMPort.DiscardInBuffer();
                        this.receiverCOMMSErrorHandler();
                        goto DetermineStatus;
                    }
                }

                int maxSignal = this.CurrentSetup.ReceiverSquelch;

                double maxSignalFrequency = 0.0;

                lock (_ReceiverCOMMSLocker) 
                {
                    ScanningLoopEnable = this._Receiver1Scanning;
                }

                while (newData.Frequency < this.CurrentSetup.ReceiverFrequencyHigh && ScanningLoopEnable)
                {
                    
                    System.Threading.Thread.Sleep(200); //Added 11/26/2013 for Rev 03

                    try
                    {
                        this.Receiver1.stepFrequency(stepSize);
                        newData = this.Receiver1.getReceiverStatusData();
                        this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { newData });
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                    }
                    catch
                    {
                        /*this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                        this.Receiver1.COMPort.DiscardInBuffer();
                        lock (_ReceiverCOMMSLocker)
                        {
                            if (!this._Receiver1Scanning)
                                break;
                        }*/
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                        this.Receiver1.COMPort.DiscardInBuffer();
                        this.receiverCOMMSErrorHandler();
                        goto DetermineStatus;
                    }

                    if (newData.SignalStrength > this.CurrentSetup.ReceiverSquelch)
                    {
                        if (newData.SignalStrength > maxSignal)
                        {
                            maxSignal = newData.SignalStrength;
                            maxSignalFrequency = newData.Frequency;
                        }
                    }
                    lock (_ReceiverCOMMSLocker)
                    {
                        ScanningLoopEnable = this._Receiver1Scanning;
                    }
                }

                if (maxSignal != this.CurrentSetup.ReceiverSquelch)
                {
                    try
                    {
                        this.Receiver1.scanFrequencies(maxSignalFrequency - 0.2, maxSignalFrequency + 0.2);
                    }
                    catch
                    {
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                        this.Receiver1.COMPort.DiscardInBuffer();
                        this.receiverCOMMSErrorHandler();
                        goto DetermineStatus;
                    }
                    try
                    {
                        newData = this.Receiver1.getReceiverStatusData();
                        this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { newData });
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                    }
                    catch
                    {
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                        this.Receiver1.COMPort.DiscardInBuffer();
                        this.receiverCOMMSErrorHandler();
                        goto DetermineStatus;
                    }

                    while (newData.SignalStrength > this.CurrentSetup.ReceiverSquelch && ScanningLoopEnable)
                    {
                        try
                        {
                            newData = this.Receiver1.getReceiverStatusData();
                            this.Invoke(new delegateUpdataReceiverDisplay(this.UpdateReceiverDisplay), new object[] { newData });
                            this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Online" });
                        }
                        catch
                        {
                            this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "COMMS Error" });
                            this.Receiver1.COMPort.DiscardInBuffer();
                            this.receiverCOMMSErrorHandler();
                            goto DetermineStatus;
                        }

                        if (this._RecordData)
                        {
                            string currentTime = DateTime.Now.ToString("HH:mm:ss");
                            string newRecord = currentTime + ",[Receiver]," + newData.Frequency.ToString("#.000") + "," + newData.SignalStrength.ToString("+000;-000") + "," + newData.Offset.ToString("+000;-000") + "\r\n";

                            try { this.Invoke(new delegateWriteToLogFileDisplay(this.UpdateLogFileDisplay), new object[] { newRecord }); }
                            catch { }

                            if (this.LogFile != null)
                            {
                                lock (_WriteFileLocker)
                                {
                                    this.LogFile.Write(newRecord);
                                }
                            }
                            System.Threading.Thread.Sleep(950);
                        }
                        lock (_ReceiverCOMMSLocker)
                        {
                            ScanningLoopEnable = this._Receiver1Scanning;
                        }
                    } //End while
                }//End if  
                System.Threading.Thread.Sleep(100);
                goto DetermineStatus;
            }
            
        }

        private void UpdateReceiverDisplay(ReceiverData newReceiverData) 
        {
            textBoxR1Frequency.Text = newReceiverData.Frequency.ToString("0.000");
            textBoxR1SignalStrength.Text = newReceiverData.SignalStrength.ToString("000");
            if (newReceiverData.AFC_ON)
            {
                textBoxR1AFCMode.Text = "ON";
            }
            else 
            {
                textBoxR1AFCMode.Text = "OFF";
            }

            if (newReceiverData.SignalStrength > this.CurrentSetup.ReceiverSquelch)
            {
                pictureBoxR1data.ImageLocation = @"LED-Green-on.png";
            }
            else 
            {
                pictureBoxR1data.ImageLocation = @"LED-Green-off.png";
            }
        }

        private void Main_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (this._RecordData) 
            {
                this._RecordData = false;
                this.LogFile.Dispose();
            }
            if (this.CurrentSetup != null)
            {
                this.CurrentSetup.writeSetup();
            }
            if (this.Receiver1Thread != null && this.Receiver1Thread.IsAlive)
            {
                lock (_ReceiverCOMMSLocker)
                {
                    this._ReceiverThreadRunning = false;
                }
                this.Receiver1Thread.Join(100);
            }
        }

        private void updateReceiverCOMMSPanel(bool comboBoxEnabled,bool enableConnectButton,string statusMessage) 
        {

                    if (comboBoxEnabled)
                    {
                        this.comboBoxR1ComPort.Enabled = true;
                    }
                    else 
                    {
                        this.comboBoxR1ComPort.Enabled = false;
                    }
                    if (enableConnectButton)
                    {
                        this.buttonR1Connect.Enabled = true;
                    }
                    else
                    {
                        this.buttonR1Connect.Enabled = false;
                    }

                    if (statusMessage.Equals("Online"))
                    {
                        this.textBoxR1Comms.Text = "Online";
                        this.textBoxR1Comms.BackColor = System.Drawing.Color.Lime;
                        this.buttonR1Scan.Enabled = true;
                    }
                    else
                    {
                        this.textBoxR1Comms.Text = statusMessage;
                        this.textBoxR1Comms.BackColor = System.Drawing.Color.Red;
                    }

                    if (!this.buttonR1Connect.Enabled) 
                    {
                        this.buttonR1Scan.Enabled = true;
                    }
        }

        private void exitToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonR1Scan_Click(object sender, EventArgs e)
        {
            buttonR1Scan.Enabled = false;

            if (buttonR1Scan.Text.Contains("Start Scanning"))
            {
                lock (_ReceiverCOMMSLocker) 
                {
                    this._Receiver1Scanning = true;
                }
                labelScanning.Visible = true;
                buttonR1Scan.Text = "Stop Scanning";
            }
            else 
            {
                lock (_ReceiverCOMMSLocker) 
                {
                    this._Receiver1Scanning = false;
                }
                labelScanning.Visible = false;
                buttonR1Scan.Text = "Start Scanning";
            }

            this.buttonR1Scan.Enabled = true;

        }

        private void buttonD1Connect_Click(object sender, EventArgs e)
        {
            if (this.buttonR1Connect.Text.Contains("Connect"))
            {
                System.Threading.Thread ConnectToReceiver1Thread = new System.Threading.Thread(ConnectToDecoder);
                ConnectToReceiver1Thread.Start();
            }
            else
            {
                MessageBox.Show("Disconnect not implemented yet. Will be added later.");
                //Disconnect from receiver
            }
        }

        void ConnectToDecoder()
        {

                    iMet_1_Decoder newDecoder = new iMet_1_Decoder();
                    if (newDecoder.connectToDecoder(this.CurrentSetup.Decoder1COMPortName))
                    {
                        this.Decoder1 = newDecoder;
                        this.Invoke(new delegateUpdateCommsDisplay(this.updateDecoderCOMMSPanel), new object[] { false, false, "Online" });

                        //Start Monitoring
                        this.Decoder1Thread = new System.Threading.Thread(MonitorDecoder);
                        this.Decoder1Thread.IsBackground = true;
                        this._Decoder1Monitor = true;
                        this.Decoder1Thread.Start();
                    }
                    else
                    {
                        MessageBox.Show("Could not detect decoder!");
                    }
            
        }

        void MonitorDecoder()
        {
                    while (this._Decoder1Monitor && !this.IsDisposed)
                    {
                        DecoderData data = this.Decoder1.getRadiosondeData();
                        try{ this.Invoke(new delegateUpdateDecoderDisplay(this.UpdateDecoderDisplay), new object[] { data }); }
                        catch { return; }

                        bool validDecoderData = false;

                        if (data.Sonde_ID != null) 
                        {
                            validDecoderData = true;
                        }

                        int signalStrength;
                        try
                        {
                            signalStrength = int.Parse(textBoxR1SignalStrength.Text);
                        }
                        catch 
                        {
                            signalStrength = 0;
                        }

                        if (this._RecordData && validDecoderData && signalStrength>=this.CurrentSetup.ReceiverSquelch)
                        {
                            DateTime newTime = DateTime.Now;
                            string newRecord = newTime.ToString("HH:mm:ss")+",[Decoder]," + data.Sonde_ID + "," + data.pressure.ToString("0.000") + "," + data.temperature.ToString("0.000") + "," + data.humidity.ToString("0.00") + "," + data.latitude.ToString("0.00000") + "," + data.longitude.ToString("0.00000") + "," + data.altitude.ToString("0.000") + "\r\n";

                            try { this.Invoke(new delegateWriteToLogFileDisplay(this.UpdateLogFileDisplay), new object[] { newRecord }); }
                            catch { }

                            if (this.LogFile != null) 
                            {
                                lock (_WriteFileLocker)
                                {
                                    try
                                    {
                                        this.LogFile.Write(newRecord);
                                    }
                                    catch (Exception writeError)
                                    {
                                        buttonStartLogging_Click(null, null);
                                        MessageBox.Show(writeError.Message, "Decoder Log Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                            }
                        }
                    }
        }

        private void UpdateLogFileDisplay(string newText) 
        {
            if (textBoxLogDisplay.TextLength > textBoxLogDisplay.MaxLength / 2) 
            {
                textBoxLogDisplay.Clear();
            }
            textBoxLogDisplay.AppendText(newText);
        }

        private void UpdateDecoderDisplay(DecoderData newDecoderData)
        {
            if (newDecoderData.Sonde_ID == null)
            {
                textBoxD1SondeID.Text = "-";
            }
            else 
            {
                textBoxD1SondeID.Text = newDecoderData.Sonde_ID;
            }
            
            if (newDecoderData.pressure == -999.99)
            {
                textBoxD1Pressure.Text = "-";
            }
            else
            {
                textBoxD1Pressure.Text = newDecoderData.pressure.ToString("#.000");
            }
            
            if (newDecoderData.temperature == -999.99)
            {
                textBoxD1Temp.Text = "-";
            }
            else
            {
                textBoxD1Temp.Text = newDecoderData.temperature.ToString("#.000");
            }
            
            if (newDecoderData.humidity == -999.99)
            {
                textBoxD1Humidity.Text = "-";
            }
            else 
            {
                textBoxD1Humidity.Text = newDecoderData.humidity.ToString("#.00");
            }
            
            if (newDecoderData.latitude != 0.0)
            {
                textBoxD1Lat.Text = newDecoderData.latitude.ToString("#.00000");
            }
            else 
            {
                textBoxD1Lat.Text = "-";
            }
            
            if (newDecoderData.longitude != 0.0)
            {
                textBoxD1Lon.Text = newDecoderData.longitude.ToString("#.00000");
            }
            else 
            {
                textBoxD1Lon.Text = "-";
            }
            
            if (newDecoderData.altitude != 0.0)
            {
                textBoxD1Alt.Text = newDecoderData.altitude.ToString("#.000");
            }
            else
            {
                textBoxD1Alt.Text = "-";
            }
        }

        private void UpdateReceiverSettingsDisplay(ReceiverSettings newReceiverSettings, bool ChangeButton, bool CancelButton)
        {
            textBoxScanLow.Text = this.CurrentSetup.ReceiverFrequencyLow.ToString("0000.000");
            textBoxScanHigh.Text = this.CurrentSetup.ReceiverFrequencyHigh.ToString("0000.000");
            textBoxSquelch.Text = this.CurrentSetup.ReceiverSquelch.ToString("+000;-000");
            
            //Get from Reciever object
            textBoxLNAGain.Text = newReceiverSettings.LNA_Gain.ToString("+00;-00");

            buttonChangeRSettings.Enabled = ChangeButton;
            buttonRCancel.Enabled = CancelButton;
        }

        private void updateDecoderCOMMSPanel(bool comboBoxEnabled, bool enableConnectButton, string DecStatus)
        {

                    if (comboBoxEnabled)
                    {
                        this.comboBoxD1ComPort.Enabled = true;
                    }
                    else
                    {
                        this.comboBoxD1ComPort.Enabled = false;
                    }
                    if (enableConnectButton)
                    {
                        this.buttonD1Connect.Enabled = true;
                    }
                    else
                    {
                        this.buttonD1Connect.Enabled = false;
                    }

                    if (DecStatus.Equals("Online"))
                    {
                        this.textBoxD1Comms.Text = "Online";
                        this.textBoxD1Comms.BackColor = System.Drawing.Color.Lime;
                    }
                    else
                    {
                        this.textBoxR1Comms.Text = "Offline";
                        this.textBoxR1Comms.BackColor = System.Drawing.Color.Red;
                    }
        }

        private void radioButtonEmailLock_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonEmailLock.Checked)
            {
                radioButtonEmailUnlock.Checked = false;
                textBoxEmailAddress.ReadOnly = true;
                groupBoxAutomaticEmail.Enabled = false;
                buttonEmailTest.Enabled = false;
                this.CurrentSetup.EmailAddress = textBoxEmailAddress.Text;
            }
        }

        private void radioButtonEmailUnlock_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButtonEmailUnlock.Checked)
            {
                radioButtonEmailLock.Checked = false;
                textBoxEmailAddress.ReadOnly = false;
                groupBoxAutomaticEmail.Enabled = true;
                buttonEmailTest.Enabled = true;
            }
        }

        private string buildLogFileName() 
        {
            string Date = DateTime.Now.Year.ToString("0000") + DateTime.Now.Month.ToString("00") + DateTime.Now.Day.ToString("00");

            //Getting the normal my docs location.
            //Removed at the request of the client.
            //string userDir = System.IO.Directory.GetParent(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments)).FullName;

            string userDir = CurrentSetup.LogFileDirectory;


            return userDir + "[" + Date + "]_" + textBoxSystemID.Text + ".log";
        }

        private void textBoxSystemID_TextChanged(object sender, EventArgs e)
        {
            textBoxLogFileName.Text = buildLogFileName();
        }

        private void buttonStartLogging_Click(object sender, EventArgs e)
        {
            if(buttonR1Scan.Text.Contains("Start")&&buttonStartLogging.Text.Contains("Start"))
            {
                DialogResult result = MessageBox.Show("Receiver is not in scan mode. Recommend setting receiver to scan mode. Do you wish to proceed?", "Receiver not in Scan Mode", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result == DialogResult.No) 
                {
                    return;
                }
            }

            buttonStartLogging.Enabled = false;

            if (buttonStartLogging.Text.Contains("Start"))
            {
                buttonStartLogging.Text = "Stop";
                textBoxSystemID.ReadOnly = true;

                this.BackColor = System.Drawing.SystemColors.ActiveCaption;

                if (System.IO.File.Exists(textBoxLogFileName.Text))
                {
                    lock (_WriteFileLocker)
                    {
                        this.LogFile = System.IO.File.AppendText(textBoxLogFileName.Text);
                    }
                }
                else 
                {
                    lock (_WriteFileLocker)
                    {
                        this.LogFile = new System.IO.StreamWriter(textBoxLogFileName.Text);
                    }
                }

                lock (_WriteFileLocker)
                {
                    this._RecordData = true;
                }
            }
            else 
            {
                buttonStartLogging.Text = "Start";
                textBoxSystemID.ReadOnly = false;

                this.BackColor = System.Drawing.SystemColors.ControlDark;

                lock (_WriteFileLocker)
                {
                    this._RecordData = false;
                    this.LogFile.Close();
                }
            }

            buttonStartLogging.Enabled = true;
        }

        private void buttonlogFileDirectory_Click(object sender, EventArgs e)
        {
            buttonlogFileDirectory.Enabled = false;

            FolderBrowserDialog logFileDialog = new FolderBrowserDialog();
            
            if (logFileDialog.ShowDialog() == DialogResult.OK)
            {
                this.CurrentSetup.LogFileDirectory = logFileDialog.SelectedPath + "\\";
                this.textBoxLogFileName.Text = buildLogFileName();
            }

            buttonlogFileDirectory.Enabled = true;
        }

        private void timerCheckForMidnight_Tick(object sender, EventArgs e)
        {
            int Hour = DateTime.Now.Hour;
            int Minute = DateTime.Now.Minute;

            

            if (Hour == 0 && Minute == 0) 
            {
                //Get the file to send
                string fileToSend = textBoxLogFileName.Text;

                //Stop logging, close file, and make new log file
                this.BackColor = System.Drawing.SystemColors.ControlDark;
                
                string newLogFileName = null;
                
                lock (_WriteFileLocker)
                {
                    if (this.LogFile != null)       //Added 11/26/13 for Rev 03
                    {
                        this.LogFile.Close();
                        //Compress File Here -- To be added later
                    }
                    else { return; }
                    
                    newLogFileName = buildLogFileName();
                    textBoxLogFileName.Text = newLogFileName;
                    this.LogFile = new System.IO.StreamWriter(newLogFileName);
                }

                this.BackColor = System.Drawing.SystemColors.ActiveCaption;
                
                //Email file
                try
                {
                    if (checkBoxEmailEnabled.Checked)
                    {
                        sendAutomaticEmailMessage(textBoxEmailAddress.Text, fileToSend);
                    }
                }
                catch { return; }
            }
        }

        private void buttonEmailTest_Click(object sender, EventArgs e)
        {
            try
            {
                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

                message.To.Add(textBoxEmailAddress.Text);
                message.From = new System.Net.Mail.MailAddress("support@intermetsystems.com");
                message.Subject = "Receiver Utility Test";
                message.Body = "This is a test of the IMS Receiver Utility sent " + DateTime.Now;

                System.Net.NetworkCredential newCredential = new System.Net.NetworkCredential("support@intermetsystems.com", "InterMet4460");

                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
                smtp.Host = "mail.intermetsystems.com";
                smtp.UseDefaultCredentials = false;
                smtp.Port = 26;

                smtp.Credentials = newCredential;
                smtp.SendAsync(message, 1);

                MessageBox.Show("Message sent successfully.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch 
            {
                MessageBox.Show("Error sending message. Check internet connection.", "Message Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void sendAutomaticEmailMessage(string EmailAddress, string FileName) 
        {
            //Compress the file here
            
            System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

            message.To.Add(EmailAddress);
            message.From = new System.Net.Mail.MailAddress("support@intermetsystems.com");
            message.Subject = "[" + DateTime.Now.ToString("yyyy/MM/dd") + "]-" + textBoxSystemID.Text;
            message.Body = "Email sent: " + DateTime.Now;

            if (System.IO.File.Exists(FileName)) 
            {
                message.Attachments.Add(new System.Net.Mail.Attachment(FileName));
            }

            System.Net.NetworkCredential newCredential = new System.Net.NetworkCredential("support@intermetsystems.com", "intermet");
            System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
            smtp.Host = "mail.intermetsystems.com";
            smtp.UseDefaultCredentials = false;
            smtp.Port = 26;
            smtp.Credentials = newCredential;

            smtp.SendAsync(message, 1);
        }

        private void buttonChangeRSettings_Click(object sender, EventArgs e)
        {
            if (buttonR1Scan.Text.Contains("Stop")) 
            {
                MessageBox.Show("You must stop scanning in order to change the receiver settings.", "Receiver Currently Scanning", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                return;
            }

            buttonChangeRSettings.Enabled = false;

            if(buttonChangeRSettings.Text.Contains("Change"))
            {
                buttonChangeRSettings.Text = "Save";
                buttonRCancel.Enabled = true;
                textBoxScanLow.ReadOnly = false;
                textBoxScanHigh.ReadOnly = false;
                textBoxSquelch.ReadOnly = false;
                textBoxLNAGain.ReadOnly = false;
                buttonChangeRSettings.Enabled = true;
            }
            else  //When button is label "Save"
            {
                buttonRCancel.Enabled = true;
                if (checkForValidReceiverSettings())
                {
                    this.CurrentSetup.ReceiverFrequencyLow = double.Parse(textBoxScanLow.Text);
                    textBoxScanLow.Text = this.CurrentSetup.ReceiverFrequencyLow.ToString("0000.000");
                    this.CurrentSetup.ReceiverFrequencyHigh = double.Parse(textBoxScanHigh.Text);
                    textBoxScanHigh.Text = this.CurrentSetup.ReceiverFrequencyHigh.ToString("0000.000");
                    this.CurrentSetup.ReceiverSquelch = int.Parse(textBoxSquelch.Text);
                    textBoxSquelch.Text = this.CurrentSetup.ReceiverSquelch.ToString("+000;-000");

                    //Change Receiver settings here

                    buttonRCancel.Enabled = false;
                    buttonChangeRSettings.Text = "Change";
                    buttonChangeRSettings.Enabled = true;

                    textBoxScanLow.ReadOnly = true;
                    textBoxScanHigh.ReadOnly = true;
                    textBoxSquelch.ReadOnly = true;
                    textBoxLNAGain.ReadOnly = true;

                    System.Threading.Thread changeReceiverSettingThread = new System.Threading.Thread(changeReceiverGainSetting);
                    changeReceiverSettingThread.Start();

                    MessageBox.Show("Receiver settings successfully changed.", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else 
                {
                    buttonChangeRSettings.Text = "Save";
                    buttonChangeRSettings.Enabled = true;
                }
            } 
        }

        private void changeReceiverGainSetting() 
        {
            if (this.Receiver1Thread.IsAlive) 
            {
                lock (_ReceiverCOMMSLocker) 
                {
                    this._ReceiverThreadRunning = false;
                }
                this.Receiver1Thread.Join();
            }

            int newLNA_Gain = int.Parse(textBoxLNAGain.Text);
            
            this.Receiver1.setLNAGain(newLNA_Gain);
            this.Receiver1.getReceiverSettings();

            if (this.Receiver1.sReceiverSettings.LNA_Gain != newLNA_Gain) 
            {
                MessageBox.Show("Error setting LNA gain for receiver.");
                return;
            }

            lock (_ReceiverCOMMSLocker) 
            {
                this._ReceiverThreadRunning = true;
            }
            this.Receiver1Thread = new System.Threading.Thread(ReceiverCommunicationLoop);
            this.Receiver1Thread.Start();
        }

        private bool checkForValidReceiverSettings() 
        {
            double lowFrequency;
            double highFrequency;
            int squelch;
            int LNA_Gain;

            try
            {
                lowFrequency = double.Parse(textBoxScanLow.Text);
                if (lowFrequency >= 1668.4 && lowFrequency <= 1700.0)
                {
                    //Intentionally left blank
                }
                else throw new Exception();
            }
            catch 
            {
                MessageBox.Show("Invalid Scan Low frequency.  Frequency must be between 1668.4 MHz and 1700.0 MHz", "Invalid Frequency", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                highFrequency = double.Parse(textBoxScanHigh.Text);
                if (highFrequency >= 1668.4 && highFrequency <= 1700.0)
                {
                    //Intentionally left blank
                }
                else throw new Exception();
            }
            catch 
            {
                MessageBox.Show("Invalid Scan High frequency.  Frequency must be between 1668.4 MHz and 1700.0 MHz", "Invalid Frequency", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                squelch = int.Parse(textBoxSquelch.Text);
                if (squelch <= 0 && squelch >= -150)
                {
                    //Valid Squelch
                }
                else throw new Exception();
            }
            catch 
            {
                MessageBox.Show("Invalid squelch setting.  Squelch must be between -150 dBm and +0 dBm.", "Invalid Squelch", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            try
            {
                LNA_Gain = int.Parse(textBoxLNAGain.Text);
                if (LNA_Gain < 50 && LNA_Gain > -10)
                {
                    //Valid LNA Gain
                }
                else throw new Exception();
            }
            catch
            {
                MessageBox.Show("Invalid LNA gain setting.  LNA gain must be between -10 dBm and +50 dBm.", "Invalid LNA Gain", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return false;
            }
            return true;
        }

        private void checkBoxEmailEnabled_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxEmailEnabled.Checked)
            {
                this.CurrentSetup.AutomaticEmail = true;
                timerCheckForMidnight.Enabled = true;
                timerCheckForMidnight.Start();
            }
            else 
            {
                this.CurrentSetup.AutomaticEmail = false;
                timerCheckForMidnight.Stop();
                timerCheckForMidnight.Enabled = false;
            }
        }

        private void buttonRCancel_Click(object sender, EventArgs e)
        {
            buttonChangeRSettings.Enabled = true;
            buttonChangeRSettings.Text = "Change";

            textBoxScanLow.Text = this.CurrentSetup.ReceiverFrequencyLow.ToString("0000.000");
            textBoxScanHigh.Text = this.CurrentSetup.ReceiverFrequencyHigh.ToString("0000.000");
            textBoxSquelch.Text = this.CurrentSetup.ReceiverSquelch.ToString("+000;-000");
            textBoxLNAGain.Text=this.Receiver1.sReceiverSettings.LNA_Gain.ToString("+00;-00");

            textBoxScanLow.ReadOnly = true;
            textBoxScanHigh.ReadOnly = true;
            textBoxSquelch.ReadOnly = true;
            textBoxLNAGain.ReadOnly = true;

            buttonRCancel.Enabled = false;
        }

        private void timerGarbageCollect_Tick(object sender, EventArgs e)
        {
            GC.Collect();
        }

        private void receiverCOMMSErrorHandler() 
        {
            bool COMPort_IsAlive = false;

            this.Receiver1.COMPort.DiscardInBuffer();
            this.Receiver1.COMPort.Close();

            this.Invoke(new delegateUpdateCommsDisplay(this.updateReceiverCOMMSPanel), new object[] { false, false, "Recovering..." });
            this.Invoke(new delegateUpdateReceiverSettings(this.UpdateReceiverSettingsDisplay), new object[] { this.Receiver1.sReceiverSettings, true, false });

            //Try to reconnect to COM port
            while (!COMPort_IsAlive) 
            {
                COMPort_IsAlive = this.Receiver1.connectToReceiver(this.CurrentSetup.Receiver1COMPortName);

            }

        }

    } //End class Main
}//End namespace IMS_Receiver_Utility
