﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace IMS_Receiver_Utility
{
    public class Setup
    {
        public string LogFileDirectory;
        public string EmailAddress;
        public bool AutomaticEmail;
        
        public string Receiver1COMPortName;
        public string Decoder1COMPortName;

        public double ReceiverFrequencyLow;
        public double ReceiverFrequencyHigh;
        public int ReceiverSquelch;

        public void getSetupFromFile(string newSetupFileDirectory) 
        {
            //Get Receiver1 COM Port
            this.Receiver1COMPortName = getReceiverCOMPort(1, newSetupFileDirectory);

            //Get Decoder1 COM Port
            this.Decoder1COMPortName = getDecoderCOMPort(1, newSetupFileDirectory);

            //Get Log File Name and Directory
            this.LogFileDirectory = getLogFileName(newSetupFileDirectory);
            this.EmailAddress = getEmailAddress(newSetupFileDirectory);
            this.AutomaticEmail = getSendEmailEnable(newSetupFileDirectory);

            this.ReceiverFrequencyLow = getLowFrequency(newSetupFileDirectory);
            this.ReceiverFrequencyHigh = getHighFrequency(newSetupFileDirectory);
            this.ReceiverSquelch = getSquelch(newSetupFileDirectory);
        }

        public void writeSetup() 
        {
            System.IO.TextWriter tw = new System.IO.StreamWriter("Setup(2).cfg");
            
            //Write Header
            tw.WriteLine("Communications");
            tw.WriteLine("--------------");
            
            //Write Receiver/Decoder COM ports
            tw.WriteLine("[Receiver1] " + this.Receiver1COMPortName);
            tw.WriteLine("[Decoder1] " + this.Decoder1COMPortName);

            //Write Log File
            tw.WriteLine("\r\n[LogFileDirectory] " + this.LogFileDirectory);
            tw.WriteLine("[EmailAddress] " + this.EmailAddress);
            if (this.AutomaticEmail)
            {
                tw.WriteLine("[AutoEmail] 1");
            }
            else 
            {
                tw.WriteLine("[AutoEmail] 0");
            }
            

            //Write Receiver Settings
            tw.WriteLine("\r\nReceiver Settings");
            tw.WriteLine("--------------");
            tw.WriteLine("[ReceiverLowFrequency] "+ this.ReceiverFrequencyLow.ToString("0000.000"));
            tw.WriteLine("[ReceiverHighFrequency] " + this.ReceiverFrequencyHigh.ToString("0000.000"));
            tw.WriteLine("[Squelch] " + this.ReceiverSquelch.ToString("+000;-000"));
            tw.Close();
            
            System.IO.File.Replace("Setup(2).cfg", "Setup.cfg", null);
            System.IO.File.Delete("Setup(2).cfg");
        }

        private string getReceiverCOMPort(int ReceiverNumber, string SetupFilePathAndDirectory) 
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFilePathAndDirectory);

            switch (ReceiverNumber) 
            {
                case 1:
                    for (int i = 0; i < SetupFileAllLines.Length; i++) 
                    {
                        if(SetupFileAllLines[i].Contains("[Receiver1]"))
                        {
                        return SetupFileAllLines[i].Replace("[Receiver1]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Receiver 1 does not exist.");
                case 2:
                    for (int i = 0; i < SetupFileAllLines.Length; i++)
                    {
                        if (SetupFileAllLines[i].Contains("[Receiver2]"))
                        {
                            return SetupFileAllLines[i].Replace("[Receiver2]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Receiver 2 does not exist.");
                case 3:
                    for (int i = 0; i < SetupFileAllLines.Length; i++)
                    {
                        if (SetupFileAllLines[i].Contains("[Receiver3]"))
                        {
                            return SetupFileAllLines[i].Replace("[Receiver3]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Receiver 3 does not exist.");
                default:
                    throw new Exception("Invalid receiver number selected by programmer.");
            }//End Switch
        }//End getReceiverCOMPort

        private string getDecoderCOMPort(int DecoderNumber, string SetupFilePathAndDirectory)
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFilePathAndDirectory);

            switch (DecoderNumber)
            {
                case 1:
                    for (int i = 0; i < SetupFileAllLines.Length; i++)
                    {
                        if (SetupFileAllLines[i].Contains("[Decoder1]"))
                        {
                            return SetupFileAllLines[i].Replace("[Decoder1]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Decoder 1 does not exist.");
                case 2:
                    for (int i = 0; i < SetupFileAllLines.Length; i++)
                    {
                        if (SetupFileAllLines[i].Contains("Decoder2]"))
                        {
                            return SetupFileAllLines[i].Replace("[Decoder2]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Decoder 2 does not exist.");
                case 3:
                    for (int i = 0; i < SetupFileAllLines.Length; i++)
                    {
                        if (SetupFileAllLines[i].Contains("[Decoder3]"))
                        {
                            return SetupFileAllLines[i].Replace("[Decoder3]", null).Trim();
                        }
                    }
                    throw new Exception("Setup file has been corrupted. The COM Port for Decoder 3 does not exist.");
                default:
                    throw new Exception("Invalid decoder number selected by programmer.");
            }//End Switch
        }//End getDecoderCOMPort

        private string getLogFileName(string SetupFilePathAndDirectory) 
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFilePathAndDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++)
            {
                if (SetupFileAllLines[i].Contains("[LogFileDirectory]"))
                {
                    return SetupFileAllLines[i].Replace("[LogFileDirectory]", null).Trim();
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }//End getLogFileName

        private string getEmailAddress(string SetupFilePathAndDirectory)
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFilePathAndDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++)
            {
                if (SetupFileAllLines[i].Contains("[EmailAddress]"))
                {
                    return SetupFileAllLines[i].Replace("[EmailAddress]", null).Trim();
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }//End getLogFileName

        private double getLowFrequency(string SetupFileDirectory) 
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFileDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++) 
            {
                if (SetupFileAllLines[i].Contains("[ReceiverLowFrequency]")) 
                {
                    string sFrequency = SetupFileAllLines[i].Replace("[ReceiverLowFrequency] ", null);
                    return double.Parse(sFrequency);
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }

        private double getHighFrequency(string SetupFileDirectory)
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFileDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++)
            {
                if (SetupFileAllLines[i].Contains("[ReceiverHighFrequency]"))
                {
                    string sFrequency = SetupFileAllLines[i].Replace("[ReceiverHighFrequency] ", null);
                    return double.Parse(sFrequency);
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }

        private int getSquelch(string SetupFileDirectory)
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFileDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++)
            {
                if (SetupFileAllLines[i].Contains("[Squelch]"))
                {
                    string sSquelch = SetupFileAllLines[i].Replace("[Squelch] ", null);
                    return int.Parse(sSquelch);
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }

        private bool getSendEmailEnable(string SetupFileDirectory) 
        {
            string[] SetupFileAllLines = System.IO.File.ReadAllLines(SetupFileDirectory);
            for (int i = 0; i < SetupFileAllLines.Length; i++)
            {
                if (SetupFileAllLines[i].Contains("[AutoEmail]"))
                {
                    string sAutoEmail = SetupFileAllLines[i].Replace("[AutoEmail] ", null);
                    int autoemail = int.Parse(sAutoEmail);
                    if (autoemail == 1) 
                    {
                        return true;
                    }
                    if (autoemail == 0)
                    {
                        return false;
                    }
                    else
                    {
                        throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
                    }
                }
            }
            throw new Exception("Setup file has been corrupted. The file path for the log file does not exist.");
        }

    }//End class Setup
}//End namespace IMS_Receiver_Utility
